package gameLogic;

import gameLogic.members.*;
import gameLogic.states.AwaitBeginning;
import gameLogic.states.GameState;
import gameLogic.states.UpdateJourneyTracker;
import gameLogic.tracker.TrackerPosition;
import gameLogic.traps.AlienTraps;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class Logic {
    private GameState state;
    private GameData gameData;

    public Logic() {
        gameData = new GameData();
        setState(new AwaitBeginning(gameData));
    }
    public GameData getGameData(){
        return gameData;
    }
    public void setGameData(GameData gameData) {
        this.gameData = gameData;
    }
    private void setNewGameData() {
        this.gameData = new gameLogic.GameData();
    }
    public void saveGame() throws IOException {
        gameData.saveGame();
    }
    public void loadGame() throws IOException, ClassNotFoundException{
        setGameData(gameData.loadGame());
        setState(new UpdateJourneyTracker(gameData));
    }
    public int getAP(){return gameData.getAP();}
    public int getIP(){return gameData.getIP();}
    public int getHealth(){return gameData.getHealth();}
    public int getHull(){return gameData.getHull();}
    public GameState getState(){
        return  state;
    }
    private void setState(GameState state) {
        this.state = state;
    }
    public CrewMember addMember(CrewMember member){
        gameData.addMember(member);
        return member;
    }
    public CrewMember getMember(int pos){return gameData.getMember(pos);}
    public List<CrewMember> getMembers(){ return gameData.getMembers();}
    public int getMembersSize(){
        return gameData.getMembersSize();
    }
    public boolean isThereAnyDoctor(){
        return gameData.isThereAnyDoctor();
    }
    public boolean isThereAnyEngineer(){
        return gameData.isThereAnyEngineer();
    }
    public void isMembersSet(){
        setState(getState().membersChosen());
    }
    public boolean isAliensAndMembersSameRoom() {
        for (CrewMember member : getMembers()) {
            if (getMemberRoom(member).getAliensSize() > 0) {
                return true;
            }
        }
        return false;
    }
    public void hideAliens(){ gameData.hideAliens();}
    public Room getRoomByNumber(int number) { return gameData.getRoomByNumber(number);}
    public Room getMemberRoom(CrewMember member){ return gameData.getMemberRoom(member);}
    public String getMemberRoomName(CrewMember member){return gameData.getMemberRoom(member).getName();}
    public void generateTracker(){
        gameData.generateTracker();
        selectMembers();
    }
    public void advanceTracker(){
        setState(getState().updateJourney());
    }
    public int numberOfAliensToSpawn(){ return gameData.numberOfAliensToSpawn();}
    public String trackerToString(){
        return gameData.trackerToString();
    }
    private void selectMembers(){
        setState(getState().selectMembers());
    }
    private void awaitStart(){setState(getState().awaitStart());}
    public List<Room> getRooms(){return gameData.getRooms();}
    public List<Room> getMemberCloseRooms(CrewMember member){return gameData.closeRoomsToMember(member);}
    public void setRoomSealed(Room room){
        setState(getState().setRoomSealed(room));
    }
    public int getAliensInMemberRoom(CrewMember member){return gameData.getMemberRoom(member).getAliensSize();}
    public boolean scienceOfficerSpecial(){ return gameData.scienceOfficerSpecial();}
    public boolean putMemberInRoom(CrewMember member, Room room){ return gameData.putMemberInRoom(member,room);}
    public boolean anyDispersers(){return gameData.anyDispersers();}
    public int getDisperserUpdate(){ return this.gameData.getDisperserUpdate();}
    public int getDetonatorUpdate(){ return this.gameData.getDetonatorUpdate();}
    public int getSealedRoomUpdate(){ return this.gameData.getSealedRoomUpdate();}
    public int getAttackDiceUpdate(){ return this.gameData.getAttackDiceUpdate();}
    public int getResultAttackUpdate(){ return this.gameData.getResultAttackUpdate();}
    public int getMovementUpdate(){ return this.gameData.getMovementUpdate();}
    public boolean isCrew(){
        return gameData.isCrew();
    }
    public void buildTracker(List<TrackerPosition> positions) {
        gameData.buildTracker(positions);
        selectMembers();
    }
    public boolean killRedShirt() {
        boolean removed = false;
        CrewMember toRemove = null;
        for (CrewMember member: gameData.getMembers()) {
            if(member instanceof RedShirt){
                toRemove = member;

            }
        }if(toRemove != null) {
            removed = gameData.killMember(toRemove);
        }
        if(removed) {
            if (gameData.getHealth() + 5 < 13) {
                for (int i = 0; i < 5; i++) {
                    gameData.addHealth();
                }
            } else {
                while (gameData.getHealth() != 12) {
                    gameData.addHealth();
                }
            }
        }
        return removed;
    }
    public void spendTrap(Room room, AlienTraps trap) {
        setState(getState().spendTrap(room,trap));
    }
    public void useDisperser(Room room){
        setState(getState().useDisperser(room));
    }
    public void attack(CrewMember member, Room room, int attack, int dices, int amountOfAliens) {
        setState(getState().attack(member,room,attack,dices,amountOfAliens));
    }
    public int randomWithRange(int max) {
        return gameData.randomWithRange(max);
    }
    public void isFirstMemberPlaced(Room room,CrewMember member) {
        setState(getState().checkFirstMemberPlaced(room,member));
    }
    public void isSecondMemberPlaced(Room room,CrewMember member) {
        setState(getState().checkSecondMemberPlaced(room,member));
    }
    public boolean fileExists(){
        File file = new File("game.txt");
        return file.exists();
    }
    public boolean fileDelete(){
        return gameData.fileDelete();
    }
    List<TrackerPosition> getTrackerPositions(){
        return gameData.getTrackerPositions();
    }
    int getTrackerCurrent(){
        return gameData.getTrackerCurrent();
    }
    int getAliensTotalAlive() {
        return gameData.getAliensTotalAlive();
    }
    public void moveMember(CrewMember member, Room old_room, Room newRoom, int turn) {
        setState(getState().moveMemberToRoom(member,old_room,newRoom,turn));
    }
    public void restartGame() {
        setNewGameData();
        awaitStart();
    }
    public void spendActionPoints(int choice) {
        setState(getState().spendActionPoints(choice));
    }
    public void spendInspirationPoints(int choice) {
        setState(getState().spendInspirationPoints(choice));
    }
    public String getOutput() {
        return gameData.getOutput();
    }
}

