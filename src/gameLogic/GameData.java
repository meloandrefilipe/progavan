package gameLogic;

import gameLogic.aliens.AlienPath;
import gameLogic.aliens.Aliens;
import gameLogic.members.*;
import gameLogic.tracker.JourneyTracker;
import gameLogic.tracker.TrackerPosition;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameData implements Serializable {
    private int AP; //Action points
    private int IP;  //inspiration points
    private int Hull; // Vida da nave
    private int Health; //Vida dos membros
    private List<CrewMember> members;
    private JourneyTracker tracker;
    private Map map;
    private Updates updates;
    private String output;

    public GameData() {
        this.AP = 5;
        this.IP = 0;
        this.Hull = 8;
        this.Health = 8;
        this.members = new ArrayList<>(2);
        this.tracker = new JourneyTracker();
        this.map = new Map();
        this.updates = new Updates();
    }


    private void updateAP( int val){
        this.AP = val;
    }
    void generateTracker(){
        this.tracker = new JourneyTracker();
    }
    public void advanceTracker(){
        tracker.advanceTracker();
        boolean commander = false;
        for (CrewMember member: members) {
            if(member instanceof Commander){
               commander = true;
            }
        }
        if (commander){
            updateAP(6);
        }else{
            updateAP(5);
        }

    }
    public int numberOfAliensToSpawn(){return tracker.numberOfAliensToSpawn();}
    public boolean isMembersSet(){
        if(members.size() == 2){
            for (CrewMember member: members) {
                if (member instanceof MoralOfficer){
                    for (int i = 0; i < 5; i++){
                        addIP();
                    }
                }if (member instanceof ShuttlePilot){
                    for (int i = 0; i < 4; i++) {
                        addHealth();
                    }
                }
            }
            return true;
        }
        return false;
    }
    int getMembersSize(){
        return members.size();
    }
    public void moveAliens(){
        HashMap<Integer,List<Integer>> nodes = new HashMap<>();
        for (Room room : getRooms()) {
            if(!room.getSealed()) {
                nodes.put(room.getNumber(),new ArrayList<>());
                for (Room closeRoom : room.getCloseRooms()) {
                    if(!closeRoom.getSealed()){
                        nodes.get(room.getNumber()).add(closeRoom.getNumber());
                    }
                }
            }
        }
        HashMap<Aliens,List<Integer>> data = new HashMap<>();
        for (Room room: getRooms()) {
            if(!room.getSealed()) {
                for (Aliens alien : room.getAliens()) {
                    int alien_room = alien.getRoom().getNumber();
                    HashMap<CrewMember, AlienPath> results = new HashMap<>();
                    boolean isInRoom = false;
                    for (CrewMember m: getMembers()) {
                        if(getMemberRoom(m).getNumber() == alien_room){
                            isInRoom = true;
                        }
                    }
                    if (!isInRoom) {
                        for (CrewMember member : getMembers()) {
                            int member_room = getMemberRoom(member).getNumber();
                            if (member_room != alien_room) {
                                AlienPath bestPath = getBestPath(nodes, alien_room, member_room);
                                results.put(member, bestPath);
                            }
                        }
                        int low = Integer.MAX_VALUE;
                        AlienPath best = new AlienPath();
                        for (CrewMember i : results.keySet()) {
                            if (low > results.get(i).getCost()) {
                                low = results.get(i).getCost();
                                best = results.get(i);
                            }
                        }
                        List<Integer> new_path = new ArrayList<>();
                        for (int i = best.getPath().size(); i > 0; i--) {
                            int rNumber = (best.getPath().get(i - 1)) + 1;
                            new_path.add(rNumber);
                        }
                        best.setPath(new_path);
                        data.put(alien,best.getPath());
                    }else{
                        data.put(alien,new ArrayList<>());
                    }
                }
            }
        }
        for (Aliens alien: data.keySet()) {
            List<Integer> path = data.get(alien);
            if(path.size() > 0) {
                Room old = alien.getRoom();
                old.removeAlien(alien);
                alien.setRoom(getRoomByNumber(path.get(1)));
                getRoomByNumber(path.get(1)).addAlien(alien);
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (Room room: getRooms()) {
            if (room.hasOrganic() > 0 && room.getAliensSize() > 0) {
                room.killAlien();
                addIP();
                room.removeDetonator();
                stringBuilder.append("Um alien foi morto com um Organic Detonator na sala ").append(room.getName());
                stringBuilder.append("\n");
            } else if (room.getMemberInRoom() > 0 && room.getAliensSize() > 0) {
                for (Aliens ignored : room.getAliens()) {
                    if (randomWithRange(6) >= 5) {
                        if (getHealth() > 0) {
                            looseHealth();
                            stringBuilder.append("Um alien atacou um membro na sala ");
                            stringBuilder.append(room.getName());
                            stringBuilder.append(" a vida dos seus tripulantes agora é: ");
                            stringBuilder.append(getHealth());
                            stringBuilder.append("\n");
                        }
                    } else {
                        stringBuilder.append("Um alien tentou atacar um membro da tripulação na sala ");
                        stringBuilder.append(room.getName());
                        stringBuilder.append(" mas falhou!");
                        stringBuilder.append("\n");
                    }
                }
            } else if (room.getAliensSize() > 0 && getHull() > 0) {
                looseHull();
                stringBuilder.append("Um alien atacou a nave, agora tem ");
                stringBuilder.append(getHull());
                stringBuilder.append(" de vida!");
                stringBuilder.append("\n");
            }
        }
        for (Room room: getRooms()) {
            if (room.getNumber() == 2) {
                for (CrewMember member : room.getMembers()) {
                    if (member instanceof  Doctor){
                        if(getHealth() < 13) {
                            addHealth();
                            stringBuilder.append("A sua vida foi aumentada em um ponto por ter o Doctor na sala ").append(room.getName());
                            stringBuilder.append("\n");
                        }else{
                            stringBuilder.append("A vida dos tripulantes já está no maximo, não pode ser adicionado um ponto bonus!");
                            stringBuilder.append("\n");
                        }
                    }
                }
            }
            if (room.getNumber() == 9) {
                for (CrewMember member : room.getMembers()) {
                    if (member instanceof  Engineer) {
                        if (getHull() < 13) {
                            addHull();
                            stringBuilder.append("A vida da nave foi aumentada em um ponto por ter o Engineer na sala ").append(room.getName());
                            stringBuilder.append("\n");
                        } else {
                            stringBuilder.append("A vida da sua nave já está no maximo, não pode ser adicionado um ponto bonus!");
                            stringBuilder.append("\n");
                        }
                    }
                }
            }
        }
        addOutput(stringBuilder.toString());
    }

    boolean scienceOfficerSpecial(){
        for (CrewMember member:members) {
            if(member instanceof ScienceOfficer){
                Room room = map.getMemberRoom(member);
                if(room.getAliensSize() == 0) {
                    for (Room cRoom : room.getCloseRooms()) {
                        if (cRoom.getAliensSize() > 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    List<Room> closeRoomsToMember(CrewMember member){
        return  map.getMemberRoom(member).getCloseRooms();
    }
    String trackerToString(){
        return this.tracker.convertToString();
    }
    public void setMembers(List<CrewMember> members){
        this.members = members;
    }
    void addMember(CrewMember member) {
        this.members.add(member);
    }
    public boolean putMemberInRoom(CrewMember member, Room room){
        return room.addMember(member);
    }
    public boolean removeMemberFromRoom(CrewMember member, Room room){
        return room.removeMember(member);
    }
    public List<CrewMember> getMembers(){
        return this.members;
    }
    CrewMember getMember(int pos){
        if(pos > members.size() || pos < 0){
            return null;
        }
        return members.get(pos);
    }
    boolean isThereAnyDoctor(){
        for (CrewMember member: members) {
            if(member instanceof Doctor){
                return true;
            }
        }
        return false;
    }
    boolean isThereAnyEngineer(){
        for (CrewMember member: members) {
            if(member instanceof Engineer){
                return true;
            }
        }
        return false;
    }
    private AlienPath getBestPath(HashMap<Integer, List<Integer>> nodes, int source, int dest){
        BreadthFirstSearch bts = new BreadthFirstSearch(12);
        return bts.getBestPath(nodes,source -1,dest -1);
    }

    int getAP() {
        return AP;
    }
    int getIP() {
        return IP;
    }
    public void addIP() {
        this.IP++;
    }
    public int getHull() {
        return Hull;
    }
    public int getHealth() {
        return Health;
    }
    public void addHealth() {
        if (Health < 12) {
            this.Health++;
        }
    }
    public void addHull(){
        if (Hull < 12) {
            this.Hull++;
        }
    }
    public void useAP(){
        if (AP > 0) {
            this.AP -= 1;
        }
    }
    public void useIP(){
        if(IP > 0) {
            this.IP -= 1;
        }
    }
    public void looseHealth(){
        if(Health > 0) {
            this.Health -= 1;
        }
    }
    private void looseHull(){
        if (Hull > 0) {
            this.Hull -= 1;
        }
    }

    public void killAllAliens(Room room){
        room.hideAliens();
    }
    public void hideAliens(){
        map.hideAliens();
    }
    List<Room> getRooms(){
        return map.getRooms();
    }
    public void removeTraps(Room room){
        for (Room r: map.getRooms()) {
            if(r.equals(room)){
                r.removeTraps();
            }
        }
    }
    boolean anyDispersers(){
        for (Room room: map.getRooms()) {
            if(room.hasDispersers()){
                return true;
            }
        }
        return false;
    }
    public Room getRoomByNumber(int number){
        return map.getRoomByNumber(number);
    }
    Room getMemberRoom(CrewMember member){return map.getMemberRoom(member);}
    public boolean sealRoom(Room room){
        return room.setSealed();
    }

    public void addAttackDice(){ this.updates.setAttackDice(this.updates.getAttackDice() +1);}
    public void addResultAttack(){ this.updates.setResultAttack(this.updates.getResultAttack() +1);}
    public void addDisperser(){this.updates.setDisperser(this.updates.getDisperser() +1);}
    public void addDetonator(){this.updates.setDetonator(this.updates.getDetonator() +1);}
    public void addSealedRoom(){this.updates.setSealedRoom(this.updates.getSealedRoom() +1);}
    public void addMovement(){this.updates.setMovement(this.updates.getMovement() +1);}

    public void spendDisperser(){this.updates.setDisperser(this.updates.getDisperser() -1);}
    public void spendDetonator(){this.updates.setDetonator(this.updates.getDetonator() -1);}
    public void spendSealedRoom(){this.updates.setSealedRoom(this.updates.getSealedRoom() -1);}

    int getDisperserUpdate(){ return this.updates.getDisperser();}
    int getDetonatorUpdate(){ return this.updates.getDetonator();}
    int getSealedRoomUpdate(){ return this.updates.getSealedRoom();}
    int getAttackDiceUpdate(){ return this.updates.getAttackDice();}
    int getResultAttackUpdate(){ return this.updates.getResultAttack();}
    int getMovementUpdate(){ return this.updates.getMovement();}

    void saveGame() throws IOException {
        ObjectOutputStream save = new ObjectOutputStream(new FileOutputStream("game.txt"));
        save.writeObject(this);
    }
    GameData loadGame() throws  IOException, ClassNotFoundException{
        ObjectInputStream load = new ObjectInputStream(new FileInputStream("game.txt"));
        return  (GameData) load.readObject();
    }
    boolean fileDelete(){
        File tempFile = new File("game.txt");
        return tempFile.delete();
    }

    public boolean isRest(){
        return tracker.isRest(tracker.getCurrent());
    }
    public boolean isCrew(){
        return tracker.isCrew();
    }
    public boolean isEnd(){
        return tracker.isEnd();
    }

    void buildTracker(List<TrackerPosition> positions) {
        this.tracker = new JourneyTracker(positions);
    }

    boolean killMember(CrewMember member) {
        return this.members.remove(member);
    }
    public int randomWithRange(int max) {
        int range = (max - 1) + 1;
        return (int)(Math.random() * range) + 1;
    }

    List<TrackerPosition> getTrackerPositions(){
        return tracker.getTrackerPositions();
    }
    int getTrackerCurrent(){
        return tracker.getCurrent();
    }
    int getAliensTotalAlive(){
        return map.getAliensTotalAlive();
    }
    public void addOutput(String newOutput){
        this.output = getOutput() + newOutput;
    }

    String getOutput() {
        String out = output;
        output = "";
        return out;
    }
}
