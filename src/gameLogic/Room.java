package gameLogic;

import gameLogic.aliens.Aliens;
import gameLogic.members.CrewMember;
import gameLogic.traps.AlienTraps;
import gameLogic.traps.OrganicDetonator;
import gameLogic.traps.ParticleDisperser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Room implements Serializable {
    private String name;
    private int number;
    private boolean sealed;
    private List<CrewMember> members;
    private List<AlienTraps> traps;
    private List<Aliens> aliens;
    private List<Room> closeRooms;

    Room(String name, int number) {
        this.name = name;
        this.number = number;
        this.sealed = false;
        this.members = new ArrayList<>();
        this.traps = new ArrayList<>();
        this.aliens = new ArrayList<>();
        this.closeRooms = new ArrayList<>();
    }

    public String getName() {
        return name;
    }
    List<Aliens> getAliens(){
        return this.aliens;
    }
    public int getNumber() {
        return number;
    }
    public int getTraps(){ return traps.size();}
    public void addTrap(AlienTraps trap){
        this.traps.add(trap);
    }
    public void removeTraps(){
        this.traps.clear();
    }
    public boolean hasDispersers(){
        for (AlienTraps trap: traps) {
            if(trap instanceof ParticleDisperser){
                return true;
            }
        }
        return false;
    }
    int hasOrganic(){
        int count = 0;
        for (AlienTraps trap: traps ) {
            if(trap instanceof OrganicDetonator){
                count++;
            }
        }
        return count;
    }
    public boolean getSealed() {
        return sealed;
    }
    boolean setSealed() {
        return this.sealed = true;
    }
    public List<CrewMember> getMembers() {
        return members;
    }
    void removeDetonator(){
        AlienTraps rTrap = new OrganicDetonator();
        for (AlienTraps trap: traps ) {
            if(trap instanceof OrganicDetonator){
                rTrap = trap;
            }
        }
        this.traps.remove(rTrap);
    }
    public void setMembers(List<CrewMember> members) {
        this.members = members;
    }
    boolean addMember(CrewMember member) { return this.members.add(member);}
    boolean removeMember(CrewMember member){
        return this.members.remove(member);
    }
    public boolean hasMember(CrewMember member){
        for (CrewMember m: members) {
            if(member.equals(m)){
                return true;
            }
        }
        return false;
    }
    public int getAliensSize(){
        return aliens.size();
    }
    public void addAlien(){this.aliens.add(new Aliens(this));}
    public void killAlien(){
        if(aliens.size() > 0){
            aliens.remove(0);
        }
    }
    void hideAliens(){
        aliens.clear();
    }
    void addCloseRoom(Room room){
        this.closeRooms.add(room);
    }
    public List<Room> getCloseRooms() {
        return closeRooms;
    }
    public int getMemberInRoom(){
        int count = 0;
        for (CrewMember m: members) {
            if (m != null){
                count++;
            }
        }
        return count;
    }
    void removeAlien(Aliens alien) {
        this.aliens.remove(alien);
    }
    void addAlien(Aliens alien){
        this.aliens.add(alien);
    }

    public boolean hasOrganics() {
        for (AlienTraps trap: traps) {
            if(trap instanceof OrganicDetonator){
                return true;
            }
        }
        return false;
    }
}
