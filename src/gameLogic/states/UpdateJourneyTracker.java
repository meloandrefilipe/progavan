package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;

public class UpdateJourneyTracker extends GameAdapter  {

    public UpdateJourneyTracker(GameData gameData) {
        super(gameData);
    }
    @Override
    public GameState looseGame() {
        return new LooseGame(getGame());
    }

    @Override
    public GameState winGame() {
        return new WinGame(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState restPhase() {
        return new SpendInspirationPoints(getGame());
    }

    @Override
    public GameState updateJourney() {
        StringBuilder stringBuilder = new StringBuilder();
        if(game.getHealth() < 1 || game.getHull() < 1){
            return looseGame();
        }else {
            game.advanceTracker();
            if (game.isCrew()) {
                int aliensToSpawn = game.numberOfAliensToSpawn();
                for (int i = 0; i < aliensToSpawn; i++) {
                    int room_number = game.randomWithRange(11);
                    Room room = game.getRoomByNumber(room_number);
                    if (!room.getSealed()) {
                        room.addAlien();
                        stringBuilder.append("Foi adicionado um alien na sala ").append(room.getName());
                        stringBuilder.append("\n");
                    } else {
                        stringBuilder.append("Não foi possivel adicionar um alien pois a sala ").append(room.getName()).append(" está encerrada!");
                        stringBuilder.append("\n");
                    }

                }
                System.out.println(stringBuilder.toString());
                return crewPhase();
            } else if (game.isRest()) {
                game.hideAliens();
                return restPhase();
            } else if (game.isEnd()) {
                return winGame();
            }
        }
        return this;
    }
}
