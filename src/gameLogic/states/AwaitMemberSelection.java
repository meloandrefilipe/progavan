package gameLogic.states;

import gameLogic.GameData;

public class AwaitMemberSelection extends GameAdapter {

    AwaitMemberSelection(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState placeFirstMember() {
        return new FirstMemberPlacement(getGame());
    }

    @Override
    public GameState membersChosen() {
        if(game.isMembersSet()){
            return placeFirstMember();
        }
        return this;
    }

}
