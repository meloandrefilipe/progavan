package gameLogic.states;
import gameLogic.Room;
import gameLogic.members.CrewMember;
import gameLogic.traps.AlienTraps;

public interface GameState {
    GameState awaitStart();
    GameState selectMembers();
    GameState placeFirstMember();
    GameState placeSecondMember();
    GameState updateTracker();
    GameState looseGame();
    GameState winGame();
    GameState crewPhase();
    GameState restPhase();
    GameState moveMember();
    GameState attackAliens();
    GameState selectRoomForTrap();
    GameState blowParticleDisperser();
    GameState sealRoom();
    GameState moveMemberToRoom(CrewMember member, Room old_room, Room newRoom, int turn);

    GameState useDisperser(Room room);

    GameState setRoomSealed(Room room);

    GameState spendTrap(Room room, AlienTraps trap);

    GameState spendActionPoints(int choice);

    GameState spendInspirationPoints(int choice);

    GameState attack(CrewMember member, Room room, int attack, int dices, int amountOfAliens);

    GameState updateJourney();

    GameState checkSecondMemberPlaced(Room room, CrewMember member);

    GameState checkFirstMemberPlaced(Room room, CrewMember member);

    GameState membersChosen();
}
