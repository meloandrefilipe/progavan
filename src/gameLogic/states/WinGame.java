package gameLogic.states;

import gameLogic.GameData;

public class WinGame extends GameAdapter {
    WinGame(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState awaitStart() {
        return new AwaitBeginning(new GameData());
    }
}
