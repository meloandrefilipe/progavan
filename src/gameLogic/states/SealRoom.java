package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;

public class SealRoom extends GameAdapter  {
    SealRoom(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState selectRoomForTrap() {
        return new SelectRoomForTrap(getGame());
    }
    @Override
    public GameState blowParticleDisperser() {
        return new ChooseParticleDisperser(getGame());
    }
    @Override
    public GameState attackAliens() {
        return new SelectMemberToAttack(getGame());
    }
    @Override
    public GameState moveMember() { return new MoveMember(getGame()); }
    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState setRoomSealed(Room room) {
        game.useAP();
        game.removeTraps(room);
        game.spendSealedRoom();
        if(game.sealRoom(room)){
            return crewPhase();
        }
        return this;
    }
}
