package gameLogic.states;

import gameLogic.GameData;

public class LooseGame extends GameAdapter {
    LooseGame(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState awaitStart() {
        return new AwaitBeginning(new GameData());
    }
}
