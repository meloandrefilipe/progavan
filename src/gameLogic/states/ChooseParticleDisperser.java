package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;

public class ChooseParticleDisperser extends GameAdapter  {
    ChooseParticleDisperser(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState sealRoom() {
        return new SealRoom(getGame());
    }
    @Override
    public GameState selectRoomForTrap() { return new SelectRoomForTrap(getGame()); }
    @Override
    public GameState attackAliens() {
        return new SelectMemberToAttack(getGame());
    }
    @Override
    public GameState moveMember() {
        return new MoveMember(getGame());
    }
    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState useDisperser(Room room){
        if(room != null){
            if(room.hasDispersers() && !room.getSealed()){
                game.killAllAliens(room);
                room.removeTraps();
                if(room.getMemberInRoom() > 0){
                    while (game.getHealth() > 0){
                        game.looseHealth();
                    }
                }
                game.useAP();
                return crewPhase();
            }
            return this;
        }
        return crewPhase();
    }
}
