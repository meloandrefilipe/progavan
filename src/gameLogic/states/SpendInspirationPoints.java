package gameLogic.states;

import gameLogic.GameData;
import gameLogic.members.CrewMember;
import gameLogic.members.Doctor;
import gameLogic.members.Engineer;

public class SpendInspirationPoints extends GameAdapter{
    SpendInspirationPoints(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState spendInspirationPoints(int choice) {
        if(choice == 0) {
            return updateTracker();
        }else if (choice == 1) {
            if(game.getHealth() < 13){
                game.addHealth();
                game.useIP();
                for (CrewMember member:game.getMembers()) {
                    if(member instanceof Doctor){
                        game.addHealth();
                    }
                }
            }
        }else if (choice == 2) {
            if(game.getHull() < 13){
                game.addHull();
                game.useIP();
                for (CrewMember member:game.getMembers()) {
                    if(member instanceof Engineer){
                        game.addHull();
                    }
                }
            }
        }else if (choice == 3) {
            game.addDetonator();
            for (int i = 0; i < 2; i++) {
                game.useIP();
            }
        }else if(choice == 4) {
            game.addMovement();
            for (int i = 0; i < 4; i++) {
                game.useIP();
            }
        }else if(choice == 5) {
            game.addDisperser();
            for (int i = 0; i < 5; i++) {
                game.useIP();
            }
        }else if(choice == 6) {
            game.addSealedRoom();
            for (int i = 0; i < 5; i++) {
                game.useIP();
            }
        }else if(choice == 7){
            game.addAttackDice();
            for (int i = 0; i < 6; i++) {
                game.useIP();
            }
        }else if(choice == 8){
            game.addResultAttack();
            for (int i = 0; i < 6; i++) {
                game.useIP();
            }
        }
        return this;
    }
}
