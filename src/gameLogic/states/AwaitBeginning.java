package gameLogic.states;

import gameLogic.GameData;

public class AwaitBeginning extends GameAdapter {
    public AwaitBeginning(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState selectMembers() {
        return new AwaitMemberSelection(getGame());
    }

    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }
}
