package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.members.CrewMember;

public class FirstMemberPlacement extends GameAdapter {
    FirstMemberPlacement(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState placeSecondMember() {
        return new SecondMemberPlacement(getGame());
    }

    @Override
    public GameState checkFirstMemberPlaced(Room room, CrewMember member) {
        for (CrewMember m : room.getMembers()){
            if(member.equals(m)){
                return placeSecondMember();
            }
        }
        return this;
    }
}
