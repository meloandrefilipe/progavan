package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.members.CrewMember;

public class SecondMemberPlacement extends GameAdapter  {
    SecondMemberPlacement(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState checkSecondMemberPlaced(Room room, CrewMember member) {
        for (CrewMember m : room.getMembers()){
            if(member.equals(m)){
                return updateTracker();
            }
        }
        return this;
    }
}
