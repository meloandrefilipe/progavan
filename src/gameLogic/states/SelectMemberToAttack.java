package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.members.Captain;
import gameLogic.members.CrewMember;

public class SelectMemberToAttack extends GameAdapter{
    SelectMemberToAttack(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }
    @Override
    public GameState sealRoom() {
        return new SealRoom(getGame());
    }
    @Override
    public GameState selectRoomForTrap() {
        return new SelectRoomForTrap(getGame());
    }
    @Override
    public GameState moveMember() {
        return new MoveMember(getGame());
    }
    @Override
    public GameState blowParticleDisperser() {
        return new ChooseParticleDisperser(getGame());
    }
    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState attack(CrewMember member, Room room, int attack, int dices, int amountOfAliens) {
        StringBuilder stringBuilder = new StringBuilder();
        if (attack >= 5 || (attack >= 3 && member instanceof Captain)) {
            if (amountOfAliens >= dices) {
                for (int i = 0; i < dices; i++) {
                    room.killAlien();
                    game.addIP();
                }
                stringBuilder.append("O membro ").append(member.getName()).append(" conseguiu eliminar ").append(dices).append(" aliens da nave!");
            } else {
                for (int i = 0; i < amountOfAliens; i++) {
                    room.killAlien();
                    game.addIP();
                }
                stringBuilder.append("O membro ").append(member.getName()).append(" conseguiu eliminar ").append(amountOfAliens).append(" aliens da nave!");
            }
        }
        else {
            stringBuilder.append("O membro ").append(member.getName()).append(" tentou um ataque mas fracassou!");
        }
        game.useAP();
        game.addOutput(stringBuilder.toString());
        return crewPhase();
    }
}
