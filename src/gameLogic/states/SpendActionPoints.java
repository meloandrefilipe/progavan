package gameLogic.states;

import gameLogic.GameData;

public class SpendActionPoints extends GameAdapter {
    SpendActionPoints(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState moveMember() {
        return new MoveMember(getGame());
    }

    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState attackAliens() {
        return new SelectMemberToAttack(getGame());
    }

    @Override
    public GameState selectRoomForTrap() {
        return new SelectRoomForTrap(getGame());
    }

    @Override
    public GameState blowParticleDisperser() {
        return new ChooseParticleDisperser(getGame());
    }

    @Override
    public GameState sealRoom() {
        return new SealRoom(getGame());
    }

    @Override
    public GameState spendActionPoints(int choice) {
        if(choice == 0) {
            game.moveAliens();
            return updateTracker();
        }else if (choice == 1) {
            return moveMember();
        }else if (choice == 2) {
            return attackAliens();
        }else if (choice == 3) {
            game.useAP();
            game.addHealth();
        }else if(choice == 4) {
            game.addHull();
            game.useAP();
        }else if(choice == 5) {
            return selectRoomForTrap();
        }else if(choice == 6) {
            return blowParticleDisperser();
        }else if(choice == 7){
            return sealRoom();
        }
        return this;
    }

}
