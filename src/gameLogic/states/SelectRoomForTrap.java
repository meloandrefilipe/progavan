package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.traps.AlienTraps;
import gameLogic.traps.OrganicDetonator;

public class SelectRoomForTrap extends GameAdapter {
    SelectRoomForTrap(GameData gameData) {
        super(gameData);
    }
    @Override
    public GameState sealRoom() { return new SealRoom(getGame()); }
    @Override
    public GameState blowParticleDisperser() {
        return new ChooseParticleDisperser(getGame());
    }
    @Override
    public GameState attackAliens() {
        return new SelectMemberToAttack(getGame());
    }
    @Override
    public GameState moveMember() {
        return new MoveMember(getGame());
    }
    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState spendTrap(Room room, AlienTraps trap) {
        room.addTrap(trap);
        if(trap instanceof OrganicDetonator){
            game.spendDetonator();
        }else{
            game.spendDisperser();
        }
        game.useAP();
        return crewPhase();
    }


}
