package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.members.CrewMember;

public class MoveMember extends GameAdapter {
    MoveMember(GameData gameData) {
        super(gameData);
    }

    @Override
    public GameState sealRoom() {
        return new SealRoom(getGame());
    }
    @Override
    public GameState selectRoomForTrap() {
        return new SelectRoomForTrap(getGame());
    }
    @Override
    public GameState blowParticleDisperser() {
        return new ChooseParticleDisperser(getGame());
    }
    @Override
    public GameState attackAliens() {
        return new SelectMemberToAttack(getGame());
    }
    @Override
    public GameState updateTracker() {
        return new UpdateJourneyTracker(getGame());
    }

    @Override
    public GameState crewPhase() {
        return new SpendActionPoints(getGame());
    }

    @Override
    public GameState moveMemberToRoom(CrewMember member, Room oldRoom,Room newRoom,int turn){
        if(turn != 0) {
            if (game.removeMemberFromRoom(member, oldRoom)) {
                game.putMemberInRoom(member, newRoom);
                if (turn == 1) {
                    game.useAP();

                }
            }
        }
        return crewPhase();
    }
}
