package gameLogic.states;

import gameLogic.GameData;
import gameLogic.Room;
import gameLogic.members.CrewMember;
import gameLogic.traps.AlienTraps;

public abstract class GameAdapter implements GameState{
    GameData game;

    public GameAdapter(GameData gameData) {
        this.game = gameData;
    }
    public GameData getGame(){
        return game;
    }

    @Override
    public GameState checkSecondMemberPlaced(Room room, CrewMember member) {
        return this;
    }

    @Override
    public GameState checkFirstMemberPlaced(Room room, CrewMember member) {
        return this;
    }

    @Override
    public GameState membersChosen() {
        return this;
    }

    @Override
    public GameState selectMembers() {
        return this;
    }

    @Override
    public GameState placeFirstMember() {
        return this;
    }

    @Override
    public GameState placeSecondMember() {
        return this;
    }

    @Override
    public GameState updateTracker() {
        return this;
    }

    @Override
    public GameState looseGame() {
        return this;
    }

    @Override
    public GameState winGame() {
        return this;
    }

    @Override
    public GameState crewPhase() {
        return this;
    }

    @Override
    public GameState restPhase() {
        return this;
    }

    @Override
    public GameState moveMember() {
        return this;
    }

    @Override
    public GameState attackAliens() {
        return this;
    }

    @Override
    public GameState selectRoomForTrap() {
        return this;
    }

    @Override
    public GameState blowParticleDisperser() {
        return this;
    }

    @Override
    public GameState sealRoom() {
        return this;
    }

    @Override
    public GameState moveMemberToRoom(CrewMember member, Room old_room, Room newRoom, int turn) {
        return this;
    }

    @Override
    public GameState useDisperser(Room room) {
        return this;
    }

    @Override
    public GameState setRoomSealed(Room room) {
        return this;
    }

    @Override
    public GameState spendTrap(Room room, AlienTraps trap) {
        return this;
    }

    @Override
    public GameState spendActionPoints(int choice) {
        return this;
    }

    @Override
    public GameState spendInspirationPoints(int choice) {
        return this;
    }

    @Override
    public GameState attack(CrewMember member, Room room, int attack, int dices, int amountOfAliens) {
        return this;
    }

    @Override
    public GameState updateJourney() {
        return this;
    }

    @Override
    public GameState awaitStart() {
        return this;
    }
}
