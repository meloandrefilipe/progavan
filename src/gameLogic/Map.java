package gameLogic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import gameLogic.members.CrewMember;

class Map implements Serializable {
    private List<Room> rooms;

    Map() {
        rooms = new ArrayList<>();
        rooms.add(new Room("Bridge",1));
        rooms.add(new Room("Sick Bay",2));
        rooms.add(new Room("Brig",3));
        rooms.add(new Room("Crew Quarters",4));
        rooms.add(new Room("Conference Room",5));
        rooms.add(new Room("Shuttle Bay",6));
        rooms.add(new Room("Weapons Bay",7));
        rooms.add(new Room("Mess Hall",8));
        rooms.add(new Room("Engineering",9));
        rooms.add(new Room("Astrometics",10));
        rooms.add(new Room("Holodeck",11));
        rooms.add(new Room("Hydroponics",12));

        //Sala 1
        rooms.get(0).addCloseRoom(rooms.get(4));
        rooms.get(0).addCloseRoom(rooms.get(7));
        //Sala 2
        rooms.get(1).addCloseRoom(rooms.get(6));
        rooms.get(1).addCloseRoom(rooms.get(5));
        rooms.get(1).addCloseRoom(rooms.get(7));
        //Sala 3
        rooms.get(2).addCloseRoom(rooms.get(4));
        rooms.get(2).addCloseRoom(rooms.get(8));
        //Sala 4
        rooms.get(3).addCloseRoom(rooms.get(7));
        rooms.get(3).addCloseRoom(rooms.get(10));
        //Sala 5
        rooms.get(4).addCloseRoom(rooms.get(0));
        rooms.get(4).addCloseRoom(rooms.get(2));
        rooms.get(4).addCloseRoom(rooms.get(7));
        rooms.get(4).addCloseRoom(rooms.get(9));
        //Sala 6
        rooms.get(5).addCloseRoom(rooms.get(1));
        rooms.get(5).addCloseRoom(rooms.get(9));
        //Sala 7
        rooms.get(6).addCloseRoom(rooms.get(1));
        rooms.get(6).addCloseRoom(rooms.get(10));
        //Sala 8
        rooms.get(7).addCloseRoom(rooms.get(0));
        rooms.get(7).addCloseRoom(rooms.get(1));
        rooms.get(7).addCloseRoom(rooms.get(3));
        rooms.get(7).addCloseRoom(rooms.get(4));
        //Sala 9
        rooms.get(8).addCloseRoom(rooms.get(2));
        rooms.get(8).addCloseRoom(rooms.get(11));
        //Sala 10
        rooms.get(9).addCloseRoom(rooms.get(4));
        rooms.get(9).addCloseRoom(rooms.get(5));
        rooms.get(9).addCloseRoom(rooms.get(11));
        //Sala 11
        rooms.get(10).addCloseRoom(rooms.get(3));
        rooms.get(10).addCloseRoom(rooms.get(6));
        //Sala 12
        rooms.get(11).addCloseRoom(rooms.get(8));
        rooms.get(11).addCloseRoom(rooms.get(9));
    }

    Room getMemberRoom(CrewMember member){
        for (Room r: rooms) {
            if(r.hasMember(member)){
                return r;
            }
        }
        return null;
    }

    List<Room> getRooms() {
        return rooms;
    }
    void hideAliens(){
        for (Room r: rooms) {
            r.hideAliens();
        }
    }
    Room getRoomByNumber(int number){
        for (Room r: rooms) {
            if (r.getNumber() == number){
                return r;
            }
        }
        return null;
    }
    int getAliensTotalAlive(){
        int total = 0;
        for (Room room:rooms) {
            total += room.getAliensSize();
        }
        return total;
    }
}
