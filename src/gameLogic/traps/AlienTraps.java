package gameLogic.traps;

import java.io.Serializable;

public class AlienTraps implements Serializable {
    private String name;

    public AlienTraps(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
