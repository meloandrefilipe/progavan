package gameLogic;

import gameLogic.aliens.AlienPath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class BreadthFirstSearch {
    private int[] pred;
    private int[] dist;
    private int vertices;
    private HashMap<Integer,List<Integer>> adj;

    BreadthFirstSearch(int vertices) {
        this.pred = new int[vertices];
        this.dist = new int[vertices];
        this.vertices = vertices;
        this.adj = new HashMap<>();
        this.adj.put(0,new ArrayList<>());
        this.adj.put(1,new ArrayList<>());
        this.adj.put(2,new ArrayList<>());
        this.adj.put(3,new ArrayList<>());
        this.adj.put(4,new ArrayList<>());
        this.adj.put(5,new ArrayList<>());
        this.adj.put(6,new ArrayList<>());
        this.adj.put(7,new ArrayList<>());
        this.adj.put(8,new ArrayList<>());
        this.adj.put(9,new ArrayList<>());
        this.adj.put(10,new ArrayList<>());
        this.adj.put(11,new ArrayList<>());
    }

    private void addEdge(int src, int dest){
        this.adj.get(src).add(dest);
    }

    private int getVertices() {
        return vertices;
    }

    private int getDist(int dest) {
        return dist[dest];
    }

    private boolean BFS(int src, int dest){
        List<Integer> queue = new ArrayList<>();
        boolean[] visited = new boolean[getVertices()];
        for (int i = 0; i < getVertices(); i++){
            visited[i] = false;
            this.dist[i] = Integer.MAX_VALUE;
            this.pred[i] = -1;
        }
        visited[src] = true;
        dist[src] = 0;
        queue.add(src);


        while (!queue.isEmpty()){
            int u = queue.get(0);
            queue.remove(0);
            for (int i = 0; i < this.adj.get(u).size(); i++){
                if(!visited[this.adj.get(u).get(i)]){
                    visited[this.adj.get(u).get(i)] = true;
                    this.dist[this.adj.get(u).get(i)] = this.dist[u]+1;
                    this.pred[this.adj.get(u).get(i)] = u;
                    queue.add(this.adj.get(u).get(i));
                    if(this.adj.get(u).get(i) == dest){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private List<Integer> getShortestDistance(int source, int dest){
        if (!BFS(source,dest)){
            return null;
        }
        List<Integer> path = new ArrayList<>(getVertices());
        int crawl = dest;
        path.add(crawl);
        while (this.pred[crawl] != -1){
            path.add(this.pred[crawl]);
            crawl = this.pred[crawl];
        }
        return path;
    }

    AlienPath getBestPath(HashMap<Integer, List<Integer>> nodes, int source, int dest){
        AlienPath data = new AlienPath();
        for (Integer key : nodes.keySet()) {
            for (Integer value: nodes.get(key)) {
                this.addEdge((key - 1),(value - 1));
            }
        }
        data.setPath(this.getShortestDistance(source,dest));
        data.setCost(this.getDist(dest));
        return data;
    }
}
