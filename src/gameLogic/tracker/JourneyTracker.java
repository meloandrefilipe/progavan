package gameLogic.tracker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JourneyTracker implements Serializable {
    private int current;
    private List<TrackerPosition> tracker;

    public JourneyTracker(){
        tracker = new ArrayList<>(15);
        current = 0;
        tracker.add(new TrackerPosition("Espaço",0,false));
        tracker.add(new TrackerPosition("2",2,true));
        tracker.add(new TrackerPosition("3",3,true));
        tracker.add(new TrackerPosition("4",4,true));
        tracker.add(new TrackerPosition("5",5,true));
        tracker.add(new TrackerPosition("D",0,false));
        tracker.add(new TrackerPosition("4",4,true));
        tracker.add(new TrackerPosition("5",5,true));
        tracker.add(new TrackerPosition("6",6,true));
        tracker.add(new TrackerPosition("D",0,false));
        tracker.add(new TrackerPosition("6",6,true));
        tracker.add(new TrackerPosition("7",7,true));
        tracker.add(new TrackerPosition("D",0,false));
        tracker.add(new TrackerPosition("8",8,true));
        tracker.add(new TrackerPosition("Terra",0,false));
        validateTracker();
    }
    public JourneyTracker(List<TrackerPosition> tracker){
        this.tracker = tracker;
        current = 0;
        validateTracker();
    }

    public int getCurrent() {
        return current;
    }
    public void advanceTracker(){
        if (current < 14) {
            current++;
        }
    }
    public int numberOfAliensToSpawn(){
        return tracker.get(current).getAliens();
    }
    private void validateTracker(){
        int i = 0;
        for (TrackerPosition pos: tracker){
            StringBuilder newName = new StringBuilder();
            if(i > 0 && i < 14){
                if (!tracker.get(i + 1).canSpawn() && i < 13) {
                    newName.append(pos.getName());
                    newName.append("A*");
                    pos.setName(newName.toString());
                } else if(!isRest(i)){
                    newName.append(pos.getName());
                    newName.append("A");
                    pos.setName(newName.toString());
                }
            }
            i++;
        }
    }
    public String convertToString(){
        StringBuilder StringBuilder = new StringBuilder();
        int i = 0;
        for (TrackerPosition pos: tracker){
            if(i == current){
                StringBuilder.append("[");
                StringBuilder.append(pos.getName());
                StringBuilder.append("]");
                if(current < 14) {
                    StringBuilder.append(" ");
                }
            }else{
                StringBuilder.append(pos.getName());
                StringBuilder.append(" ");
            }
            i++;
        }
        return StringBuilder.toString();
    }

    public boolean isRest(int pos){
        return tracker.get(pos).isRest();
    }
    public boolean isEnd(){
        return tracker.get(current).isEnd();
    }
    public boolean isCrew(){
        return tracker.get(current).isCrew();
    }

    public List<TrackerPosition> getTrackerPositions() {
        return tracker;
    }

}
