package gameLogic.tracker;

import java.io.Serializable;

public class TrackerPosition implements Serializable {
    private int aliens;
    private boolean spawn;
    private String name;

    public TrackerPosition(String name, int aliens, boolean spawn) {
        this.name = name;
        this.aliens = aliens;
        this.spawn = spawn;
    }

    public boolean isRest(){
        return this.name.equals("D");
    }
    public boolean isEnd(){
        return this.name.equals("Terra");
    }
    public boolean isCrew(){
        return this.canSpawn();
    }
    public boolean isStart(){
        return this.name.equals("Espaço");
    }
    public String getName() {
        return name;
    }

    public int getAliens() {
        return aliens;
    }

    public boolean canSpawn() {
        return spawn;
    }

    void setName(String name) {
        this.name = name;
    }
}
