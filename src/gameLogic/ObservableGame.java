package gameLogic;

import gameLogic.members.*;
import gameLogic.states.GameState;
import gameLogic.tracker.TrackerPosition;
import gameLogic.traps.AlienTraps;

import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.List;

public class ObservableGame extends PropertyChangeSupport {
    private Logic game;

    public ObservableGame(Logic game) {
        super(game);
        this.game = game;
    }

    @Override
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void updateGame(){
        firePropertyChange(null,null,null);
    }

    public void spendInspirationPoints(int choice) {
        game.spendInspirationPoints(choice);
    }
    public GameData getGameData(){
        return game.getGameData();
    }
    public void saveGame() throws IOException {
        game.saveGame();
    }
    public void loadGame() throws IOException, ClassNotFoundException{
        game.loadGame();
    }
    public int getAP(){return game.getAP();}
    public int getIP(){return game.getIP();}
    public int getHealth(){
        return game.getHealth();
    }
    public int getHull(){return game.getHull();}
    public GameState getState(){
        return  game.getState();
    }
    public void addMember(CrewMember member){
        game.addMember(member);
    }
    public CrewMember getMember(int pos){return game.getMember(pos);}
    public List<CrewMember> getMembers(){ return game.getMembers();}
    public boolean isThereAnyDoctor(){
        return game.isThereAnyDoctor();
    }
    public boolean isThereAnyEngineer(){
        return game.isThereAnyEngineer();
    }
    public void isMembersSet(){
        game.isMembersSet();
    }
    public boolean isAliensAndMembersSameRoom() {
        return game.isAliensAndMembersSameRoom();
    }
    public Room getMemberRoom(CrewMember member){ return game.getMemberRoom(member);}
    public void generateTracker(){
        game.generateTracker();
    }
    public void advanceTracker(){
        game.advanceTracker();
    }
    public List<Room> getRooms(){return game.getRooms();}
    public  List<Room> getMemberCloseRooms(CrewMember member){return game.getMemberCloseRooms(member);}
    public void setRoomSealed(Room room){
        game.setRoomSealed(room);
    }
    public int getAliensInMemberRoom(CrewMember member){return game.getAliensInMemberRoom(member);}
    public boolean scienceOfficerSpecial(){ return game.scienceOfficerSpecial();}
    public void putMemberInRoom(CrewMember member, Room room){
        game.putMemberInRoom(member, room);
    }
    public void removeMemberFromRoom(CrewMember member, Room old, Room newRoom, int turn){
        game.moveMember(member,old,newRoom,turn);
    }
    public boolean anyDispersers(){return game.anyDispersers();}
    public void buildTracker(List<TrackerPosition> positions) {
        game.buildTracker(positions);
    }
    public boolean killRedShirt() {
        return game.killRedShirt();
    }
    public void spendTrap(Room room, AlienTraps trap) {
        game.spendTrap(room,trap);
    }
    public void useDisperser(Room room){
        game.useDisperser(room);
    }
    public void attack(CrewMember member, Room room, int attack, int dices, int amountOfAliens) {
        game.attack(member,room,attack,dices,amountOfAliens);
    }
    public int randomWithRange(int max) {
        return game.randomWithRange(max);
    }
    public void isFirstMemberPlaced(Room room, CrewMember member) {
        game.isFirstMemberPlaced(room, member);
    }
    public void isSecondMemberPlaced(Room room, CrewMember member) {
        game.isSecondMemberPlaced(room, member);
    }
    public boolean fileExists(){
        return game.fileExists();
    }
    public boolean fileDelete(){
        return game.fileDelete();
    }
    public int getDisperserUpdate(){ return game.getDisperserUpdate();}
    public int getDetonatorUpdate(){ return game.getDetonatorUpdate();}
    public int getSealedRoomUpdate(){ return game.getSealedRoomUpdate();}
    public int getAttackDiceUpdate(){ return game.getAttackDiceUpdate();}
    public int getResultAttackUpdate(){ return game.getResultAttackUpdate();}
    public int getMovementUpdate(){ return game.getMovementUpdate();}
    public List<TrackerPosition> getTrackerPositions(){
        return game.getTrackerPositions();
    }
    public int getTrackerCurrent(){
        return game.getTrackerCurrent();
    }
    public int getAliensTotalAlive() {
        return game.getAliensTotalAlive();
    }
    public void spendActionPoints(int choice){game.spendActionPoints(choice);}

    public String getOutput() {
        return game.getOutput();
    }
}


