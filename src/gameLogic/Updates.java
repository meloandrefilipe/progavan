package gameLogic;

import java.io.Serializable;

public class Updates implements Serializable {
    private int disperser;
    private int detonator;
    private int sealedRoom;
    private int attackDice;
    private int resultAttack;
    private int movement;

    public Updates() {
        this.disperser = 0;
        this.detonator = 0;
        this.sealedRoom = 0;
        this.attackDice = 0;
        this.resultAttack = 0;
        this.movement = 0;
    }

    int getDisperser() {
        return disperser;
    }

    void setDisperser(int disperser) {
        this.disperser = disperser;
    }

    int getDetonator() {
        return detonator;
    }

    void setDetonator(int detonator) {
        this.detonator = detonator;
    }

    int getSealedRoom() {
        return sealedRoom;
    }

    void setSealedRoom(int sealedRoom) {
        this.sealedRoom = sealedRoom;
    }

    int getAttackDice() {
        return attackDice;
    }

    void setAttackDice(int attackDice) {
        this.attackDice = attackDice;
    }

    int getResultAttack() {
        return resultAttack;
    }

    void setResultAttack(int resultAttack) {
        this.resultAttack = resultAttack;
    }

    int getMovement() {
        return movement;
    }

    void setMovement(int movement) {
        this.movement = movement;
    }
}
