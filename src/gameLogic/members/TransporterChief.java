package gameLogic.members;

public class TransporterChief extends CrewMember {
    public TransporterChief() {
        super("Transporter Chief", 1, 0);
    }
}
