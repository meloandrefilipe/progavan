package gameLogic.members;

public class Captain extends CrewMember {
    public Captain() {
        super("Captain", 1, 1);
    }
}
