package gameLogic.members;

public class NavigationOfficer extends CrewMember {

    public NavigationOfficer() {
        super("Navigation Officer",1,2);
    }
}
