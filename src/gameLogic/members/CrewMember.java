package gameLogic.members;


import java.io.Serializable;

public class CrewMember implements Serializable {
    private String name;
    private int attack;
    private int movement;

    public CrewMember(String name, int attack, int movement) {
        this.name = name;
        this.attack = attack;
        this.movement = movement;
    }



    public int getAttack() {
        return attack;
    }

    public int getMovement() {
        return movement;
    }

    public String getName() {
        return name;
    }
}
