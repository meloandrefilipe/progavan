package gameLogic.members;

public class ShuttlePilot  extends CrewMember{
    public ShuttlePilot() {
        super("Shuttle Pilot", 1, 1);
    }
}
