package gameLogic.aliens;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AlienPath implements Serializable {
    private int cost;
    private List<Integer> path;

    public AlienPath() {
        this.cost = 0;
        this.path = new ArrayList<>();
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public List<Integer> getPath() {
        return path;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }
}
