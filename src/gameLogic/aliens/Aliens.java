package gameLogic.aliens;

import gameLogic.Room;

import java.io.Serializable;

public class Aliens implements Serializable {
    private Room room;

    public Aliens(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return this.room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
