package userInterface.text;

import gameLogic.Logic;
import gameLogic.Room;
import gameLogic.members.*;
import gameLogic.states.*;
import gameLogic.tracker.TrackerPosition;
import gameLogic.traps.AlienTraps;
import gameLogic.traps.OrganicDetonator;
import gameLogic.traps.ParticleDisperser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class TextInterface {
    private Logic game;
    private boolean quit = false;
    TextInterface(Logic game) {
        this.game = game;
    }

    void run(){
        while (!quit) {
            GameState state = game.getState();
            makeEnters();
            if (state instanceof AwaitBeginning) {
                uiAwaitBeginning();
            } else if (state instanceof AwaitMemberSelection) {
                uiAwaitMemberSelection();
            } else if(state instanceof FirstMemberPlacement){
                uiSelectFirstMember();
            }else if (state instanceof SecondMemberPlacement) {
                uiSelectSecondMember();
            }else if (state instanceof UpdateJourneyTracker){
                uiUpdateJourneyTracker();
            }else if (state instanceof SpendActionPoints){
                uiSpendActionPoints();
            }else if (state instanceof SpendInspirationPoints){
                uiSpendInspirationPoints();
            }else if (state instanceof WinGame){
                uiWinGame();
            }else if (state instanceof LooseGame){
                uiLooseGame();
            }else if(state instanceof MoveMember){
                uiMoveMember();
            }else if (state instanceof ChooseParticleDisperser){
                uiBlowDisperser();
            }else if(state instanceof SealRoom){
                uiSelectRoomToSeal();
            }else if (state instanceof SelectMemberToAttack){
                uiAttackAliens();
            }else if(state instanceof SelectRoomForTrap) {
                uiSetTrap();
            }
        }
    }

    private void uiAwaitBeginning(){
        Scanner SC = new Scanner(System.in);
        boolean choose = false;
        System.out.println("Bem-vindo/a ao Destination Earth!");
        String choice;
        while (!quit) {
            System.out.println("[I] - Iniciar Jogo");
            if(game.fileExists()) {
                System.out.println("[C] - Carregar Jogo");
                System.out.println("[A] - Apagar Jogo");
            }
            System.out.println("[S] - Sair");
            System.out.println("Selecione uma opção: ");
            choice = SC.nextLine().toUpperCase().trim();
            while (!choose){
                if(choice.length() > 0) {
                    if (choice.charAt(0) == 'I' || (choice.charAt(0) == 'C' && game.fileExists() )|| choice.charAt(0) == 'S' || (choice.charAt(0) == 'A' && game.fileExists() )) {
                        choose = true;
                    }
                }
                if (!choose) {
                    System.out.println("Selecione uma opção válida: ");
                    choice = SC.nextLine().toUpperCase().trim();
                }
            }
            switch (choice.charAt(0)) {
                case 'I':
                    System.out.println("Pretende criar o seu Journey Tracker? (s/N)");
                    String awser = "";
                    boolean isawser = false;
                    int val = 0;
                    while (!isawser){
                        awser = SC.nextLine().toUpperCase().trim();
                        if (awser.length() == 0){
                            game.generateTracker();
                            isawser = true;
                        } else if(awser.charAt(0) == 'S' || awser.charAt(0) == 'N'){
                            isawser = true;
                        }
                        if (!isawser){
                            System.out.println("Insira uma opção válida!");
                        }
                    }
                    if (awser.length() == 0){
                        game.generateTracker();
                    }else {
                        switch (awser.charAt(0)) {
                            case 'S':
                                List<TrackerPosition> positions = new ArrayList<>(15);
                                Scanner ASC_text = new Scanner(System.in);
                                Scanner ASC_int = new Scanner(System.in);
                                String alien_text;
                                int alien_int;
                                int total_aliens = 0;
                                boolean exit = false;
                                positions.add(new TrackerPosition("Espaço",0,false));
                                while (val <= 13){
                                    do {
                                        if (positions.get(val).canSpawn()) {
                                            System.out.println("[D] - Jornada de descanço");
                                        }
                                        if (total_aliens < 15) {
                                            System.out.println("[T] - Jornada da tripulação");
                                        }
                                        System.out.println("Selecione uma opção para a jornada " + (val + 1) + ": ");
                                        alien_text = ASC_text.nextLine().toUpperCase().trim();
                                        if(alien_text.length() > 0) {
                                            if ((alien_text.charAt(0) == 'D' && positions.get(val).canSpawn()) || (alien_text.charAt(0) == 'T' && total_aliens < 15)) {
                                                exit = true;
                                            }
                                        }
                                    } while (!exit);

                                    switch (alien_text.charAt(0)){
                                        case 'D':
                                            positions.add(new TrackerPosition("D",0,false));
                                            total_aliens = 0;
                                            break;
                                        case 'T':
                                            do {
                                                System.out.println("Indique o numero de aliens para a jornada [1," + (15 - (total_aliens)) + "]:");
                                                while (!ASC_int.hasNextInt()) {
                                                    System.out.println("Insira um valor entre 0 e " + (15 - (total_aliens)) + "!");
                                                    ASC_int.next();
                                                }
                                                alien_int = ASC_int.nextInt();
                                            } while (alien_int <= 0 || alien_int > 15 - total_aliens);

                                            total_aliens += alien_int;
                                            positions.add(new TrackerPosition(String.valueOf(alien_int),alien_int,true));
                                            break;
                                    }
                                    val++;
                                }
                                positions.add(new TrackerPosition("Terra",0,false));
                                game.buildTracker(positions);
                                break;
                            case 'N':
                                game.generateTracker();
                                break;
                        }
                    }
                    System.out.println(game.trackerToString());
                    return;
                case 'C':
                    try {
                        game.loadGame();
                        System.out.println("O seu jogo foi carregado!");
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    return;
                case 'A':
                    if(game.fileDelete()){
                        System.out.println("O ficheiro do jogo foi apagado!");
                    }else{
                        System.out.println("Não foi possivel apagar o ficheiro!");
                    }
                    return;
                case 'S':
                    quit = true;
                    break;
            }
        }
    }
    private void uiAwaitMemberSelection(){
        boolean crew = false;
        Scanner SCint = new Scanner(System.in);
        while (!crew){
            System.out.println("Membros da tripulação:");
            int old = -1;
            for (int i = 0; i < 2; i++){
                if(old != 1) { System.out.println("[1] - Doctor"); }
                if(old != 2) { System.out.println("[2] - Comm's Officer"); }
                if(old != 3) { System.out.println("[3] - Red Shirt"); }
                if(old != 4) { System.out.println("[4] - Science Officer"); }
                if(old != 5) { System.out.println("[5] - Engineer"); }
                if(old != 6) { System.out.println("[6] - Captain"); }
                if(old != 7) { System.out.println("[7] - Commander"); }
                if(old != 8) { System.out.println("[8] - Transporter Chief"); }
                if(old != 9) { System.out.println("[9] - Moral Officer"); }
                if(old != 10) {System.out.println("[10] - Security Officer"); }
                if(old != 11) { System.out.println("[11] - Navigation Officer"); }
                if(old != 12) { System.out.println("[12] - Shuttle Pilot"); }
                System.out.println("[0] - Selecionar Aleatóriamente");
                System.out.println("Selecione o "+(i+1)+"º membro:");
                while (!SCint.hasNextInt()){
                    System.out.println("Insira um valor entre 0 e 12!");
                    SCint.next();
                }
                int member_number = SCint.nextInt();

                while(member_number < 0 || member_number > 12 || member_number == old){
                    System.out.println("Selecione uma opção entre 0 e 12: ");
                    member_number = SCint.nextInt();
                }
                if(member_number == 0){
                    member_number = game.randomWithRange(12);
                    if(member_number == old){
                        while (member_number == old) {
                            member_number = game.randomWithRange(12);
                        }
                    }
                }
                old = member_number;
                CrewMember member = null;
                switch (member_number){
                    case 1:
                        member = game.addMember(new Doctor());
                        break;
                    case 2:
                        member = game.addMember(new CommsOfficer());
                        break;
                    case 3:
                        member = game.addMember(new RedShirt());
                        break;
                    case 4:
                        member = game.addMember(new ScienceOfficer());
                        break;
                    case 5:
                        member = game.addMember(new Engineer());
                        break;
                    case 6:
                        member = game.addMember(new Captain());
                        break;
                    case 7:
                        member = game.addMember(new Commander());
                        break;
                    case 8:
                        member = game.addMember(new TransporterChief());
                        break;
                    case 9:
                        member = game.addMember(new MoralOfficer());
                        break;
                    case 10:
                        member = game.addMember(new SecurityOfficer());
                        break;
                    case 11:
                        member = game.addMember(new NavigationOfficer());
                        break;
                    case 12:
                        member = game.addMember(new ShuttlePilot());
                        break;
                }
                if(member != null) {
                    System.out.println("Foi selecionado o membro: " + member.getName());
                }
                if(game.getMembersSize() == 2) {
                    crew = true;
                }
            }
        }
        game.isMembersSet();
    }
    private void uiSelectFirstMember(){
        CrewMember member = game.getMember(0);
        System.out.println("Colocar o "+member.getName() + " em posição!");
        Scanner SC = new Scanner(System.in);
        boolean choose = false;
        System.out.println("[A] - Colocar Aleatóriamente");
        System.out.println("[E] - Escolher Posição");
        System.out.println("Escolha uma opção: ");
        String option = "";
        while (!choose) {
            option = SC.nextLine().toUpperCase().trim();
            if(option.length() > 0) {
                if (option.charAt(0) == 'A' || option.charAt(0) == 'E') {
                    choose = true;
                }
            }
            if(!choose){
                System.out.println("Insira uma opção válida (A ou E)!");
            }

        }
        int room_number = 0;
        switch (option.charAt(0)) {
            case 'A':
                room_number = game.randomWithRange(12);
                break;
            case 'E':
                for (Room room: game.getRooms()) {
                    System.out.println("["+room.getNumber()+"] - "+room.getName());
                }
                System.out.println("Indique a sala pertendida:");
                room_number = SC.nextInt();
                while (room_number < 1 || room_number > 12) {
                    System.out.println("Só existem salas de 1 a 12!");
                    room_number = SC.nextInt();
                }
                break;
        }
        Room room = null;

        if (room_number > 0 && room_number < 13) {
            room = game.getRoomByNumber(room_number);
            member = game.getMember(0);
            if(game.putMemberInRoom(member, room)){
                System.out.println("O membro " +member.getName() + " foi colocado na sala " + room.getName() + "!");
            }
        }
        if(room != null) {
            game.isFirstMemberPlaced(room,member);
        }
    }
    private void uiSelectSecondMember(){
        CrewMember member = game.getMember(1);
        System.out.println("Colocar o "+member.getName() + " em posição!");
        Scanner SC = new Scanner(System.in);
        boolean choose = false;
        System.out.println("[A] - Colocar Aleatóriamente");
        System.out.println("[E] - Escolher Posição");
        System.out.println("Escolha uma opção: ");
        String option = "";
        boolean end = false;
        while (!choose) {
            option = SC.nextLine().toUpperCase().trim();
            if(option.length() > 0) {
                if (option.charAt(0) == 'A' || option.charAt(0) == 'E') {
                    choose = true;
                }
            }
            if(!choose){
                System.out.println("Insira uma opção válida (A ou E)!");
            }

        }
        int room_number = 0;
        switch (option.charAt(0)) {
            case 'A':
                room_number = game.randomWithRange(12);
                break;
            case 'E':
                for (Room room: game.getRooms()) {
                    System.out.println("["+room.getNumber()+"] - "+room.getName());
                }
                System.out.println("Indique a sala pertendida:");
                room_number = SC.nextInt();
                while (room_number < 1 || room_number > 12) {
                    System.out.println("Só existem salas de 1 a 12!");
                    room_number = SC.nextInt();
                }
                break;
        }
        Room room = null;
        if (room_number > 0 && room_number < 13) {
            room = game.getRoomByNumber(room_number);
            game.putMemberInRoom(member, room);
            System.out.println("O membro " + member.getName() + " foi colocado na sala " + room.getName()+ "!");
        }
        if(room != null) {
            game.isSecondMemberPlaced(room,member);
        }
    }
    private void uiUpdateJourneyTracker(){
        Scanner SC = new Scanner(System.in);
        String choice = "";
        boolean choosen = false;
        System.out.println("Journey Tracker");
        System.out.println(game.trackerToString());
        System.out.println("[A] - Avançar para a próxima jornada");
        System.out.println("[G] - Guardar Jogo");
        System.out.println("[S] - Sair");
        System.out.println("Escolha uma opção: ");
        while (!choosen) {
            choice = SC.nextLine().toUpperCase().trim();
            if(choice.length() >0) {
                if ((Character.isLetter(choice.charAt(0))) && (choice.charAt(0) == 'A' || choice.charAt(0) == 'G' || choice.charAt(0) == 'S')) {
                    choosen = true;
                }
            }
            if(!choosen){
                System.out.println("Selecione uma opção válida!");
            }
        }
        if (choice.charAt(0) == 'A') {
            System.out.println("antes: "+game.getState());
            game.advanceTracker();
            System.out.println("depois: "+ game.getState());
            int aliensToSpawn = game.numberOfAliensToSpawn();
            if (aliensToSpawn < 0) {
                System.out.println("Este jogo está corrompido! A terminar...");
                quit = true;
            }
            if(game.isCrew()){
                redShirtSpecial();
            }
        } else if (choice.charAt(0) == 'G'){
            try {
                game.saveGame();
                System.out.println("O seu jogo foi guardado em game.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            quit = true;
        }
    }
    private void uiSpendInspirationPoints() {
        int IP = game.getIP();
        System.out.println("Jornada de descanço!");
        System.out.println(game.trackerToString());
        updatePlayer();
        game.hideAliens();
        System.out.println("Todos os Aliens fugiram devolta para os seus esconderijos!");
        if(IP == 0){
            System.out.println("Ainda não tem Inspiration Points disponíveis!");
            System.out.println("Boa sorte para a proxíma!");
        }else {
            IP = game.getIP();
            if(IP > 0) {
                System.out.println("Aqui pode gastar todos os seus " + IP + " Inspiration Points nos updates que pertender!");
                if (game.getHealth() < 12) {
                    System.out.println("[1] - Adicionar um de vida (Custo: 1)");
                }
                if (game.getHull() < 12) {
                    System.out.println("[2] - Reparar a nave em um ponto (Custo: 1)");
                }
                if (IP >= 2) {
                    System.out.println("[3] - Comprar um Organic Detonator (Custo: 2)");
                }
                if (IP >= 4) {
                    System.out.println("[4] - Adicionar um ao movimento (Custo: 4)");
                }
                if (IP >= 5) {
                    System.out.println("[5] - Comprar um Particle Disperser (Custo: 5)");
                    System.out.println("[6] - Comprar chave para fechar uma sala (Custo: 5)");
                }
                if (IP >= 6) {
                    System.out.println("[7] - Ganhar um dado de ataque (Custo: 6)");
                    System.out.println("[8] - Adicionar 1 ao resultado do ataque (Custo: 6)");
                }
                System.out.println("[0] - Sair");
                System.out.println("Selecione uma opção: ");
                Scanner SC = new Scanner(System.in);
                int choice = 0;
                boolean choosen = false;
                while (!choosen) {
                    while (!SC.hasNextInt()) {
                        System.out.println("Insira um valor entre 0 e 8!");
                        SC.next();
                    }
                    choice = SC.nextInt();
                    if (choice == 0) {
                        choosen = true;
                    }
                    if ((choice == 1) && game.getHealth() < 12) {
                        choosen = true;
                    }
                    if ((choice == 2) && game.getHull() < 12) {
                        choosen = true;
                    }
                    if (IP >= 2 && (choice >= 1 && choice <= 3)) {
                        choosen = true;
                    }
                    if (IP >= 4 && (choice >= 1 && choice <= 4)) {
                        choosen = true;
                    }
                    if (IP >= 5 && (choice >= 1 && choice <= 6)) {
                        choosen = true;
                    }
                    if (IP >= 6 && (choice >= 1 && choice <= 8)) {
                        choosen = true;
                    }
                    if (!choosen) {
                        System.out.println("Insira uma opção válida!");
                    }
                }
                int health = game.getHealth();
                int hull = game.getHull();
                game.spendInspirationPoints(choice);
                if (choice == 1) {
                    int points = game.getHealth() - health;
                    if(points >1){
                        System.out.println("Devido a existir um Engineer na tripulação, tem mais um ponto de vida da nave grátis!");
                    }else if (points < 1){
                        System.out.println("A vida dos tripulantes já está no maximo "+ game.getHealth()+ ", não foram gastos inspiration points!");
                    }else {
                        System.out.println("A vida dos membros foi aumentada para " + game.getHealth() + " e agora tem " + game.getIP() + " inspiration points.");
                    }
                }else if (choice == 2) {
                int points = game.getHull()-hull;
                if(points >1){
                    System.out.println("Devido a existir um Engineer na tripulação, tem mais um ponto de vida da nave grátis!");
                }else if (points < 1){
                    System.out.println("A vida da nave já está no máximo "+game.getHull() +", não foram gastos inspiration points!");
                }else {
                    System.out.println("A vida da nave foi aumentada para " + game.getHull() + " e agora tem " + game.getIP() + " inspiration points.");
                }
                }else if (choice == 3) {
                    System.out.println("Agora tem " + game.getDetonatorUpdate() + " organic detonators e " + game.getIP() + " inspiration points.");
                }else if(choice == 4) {
                    System.out.println("Agora os membros podem mover-se mais " + game.getMovementUpdate() + " salas e tem " + game.getIP() + " inspiration points.");
                }else if(choice == 5) {
                    System.out.println("Agora tem " + game.getDisperserUpdate() + " particle disperser e " + game.getIP() + " inspiration points.");
                }else if(choice == 6) {
                    System.out.println("Agora pode fechar " + game.getSealedRoomUpdate() + " salas e tem " + game.getIP() + " inspiration points.");
                }else if(choice == 7){
                    System.out.println("Agora são lançados " + game.getAttackDiceUpdate() + " dados a cada ataque e tem " + game.getIP() + " inspiration points.");
                }else if(choice == 8){
                    System.out.println("Agora faz um dano extra de " + game.getResultAttackUpdate() + " e tem " + game.getIP() + " inspiration points.");
                }
            }else{
                System.out.println("Já gastou todos os seus Inspiration Points!");
                game.spendInspirationPoints(0);
            }
        }
    }
    private void uiSpendActionPoints() {
        Scanner SC = new Scanner(System.in);
        if(game.getAP() > 0){
            System.out.println("Jornada da tripulação!");
            System.out.println(game.trackerToString());
            updatePlayer();
            System.out.println("Tem "+game.getAP()+" Action Points disponíveis!");
            System.out.println("[1] - Mover");
            if(game.isAliensAndMembersSameRoom()) {
                System.out.println("[2] - Atacar");
            }
            if((game.isThereAnyDoctor()) && game.getHealth() < 12 ) {
                System.out.println("[3] - Adicionar um de vida");
            }
            if((game.isThereAnyEngineer()) && game.getHull() < 12 ) {
                System.out.println("[4] - Reparar a nave em um ponto");
            }
            if(game.getDisperserUpdate() > 0 || game.getDetonatorUpdate() > 0) {
                System.out.println("[5] - Colocar Armadilha");
            }
            if(game.anyDispersers()) {
                System.out.println("[6] - Detonar Particle Disperser");
            }
            if(game.getSealedRoomUpdate() > 0) {
                System.out.println("[7] - Fechar uma sala");
            }
            System.out.println("[0] - Sair");
            System.out.println("Selecione uma opção: ");

            int choice = 0;
            boolean choosen = false;
            while (!choosen) {
                while (!SC.hasNextInt()) {
                    System.out.println("Insira um valor entre 0 e 7!");
                    SC.next();
                }
                choice = SC.nextInt();
                if(choice == 0 || choice == 1){
                    choosen = true;
                }
                if((choice == 2 && game.isAliensAndMembersSameRoom())){
                    choosen = true;
                }
                if((game.isThereAnyDoctor()) && choice == 3 && game.getHealth() < 12 ) {
                    choosen = true;
                }
                if((game.isThereAnyEngineer()) && choice == 4 && game.getHull() < 12) {
                    choosen = true;
                }
                if((game.getDisperserUpdate() > 0 || game.getDetonatorUpdate() > 0) && choice == 5) {
                    choosen = true;
                }
                if (game.anyDispersers() && choice == 6){
                    choosen = true;
                }
                if (game.getSealedRoomUpdate() > 0 && choice == 7){
                    choosen = true;
                }
                if (!choosen){
                    System.out.println("Deve inserir uma opção válida!");
                }
            }
            game.spendActionPoints(choice);
            if(choice == 0){
                System.out.println(game.getOutput());
            }
        }else {
            game.spendActionPoints(0);
            System.out.println(game.getOutput());
            redShirtSpecial();
        }
    }
    private void uiMoveMember(){
        Scanner SC = new Scanner(System.in);
        int i = 1;
        for (CrewMember member: game.getMembers()) {
            System.out.println("["+i+"] - "+member.getName() + "(Localização: "+game.getMemberRoomName(member)+")");
            i++;
        }
        System.out.println("Selecione um membro: ");
        int member_choice;
        while (true){
            member_choice = SC.nextInt();
            if(member_choice >= 0 && member_choice < game.getMembersSize() +1){
                break;
            }else{
                System.out.println("Insira uma opção válida:");
            }
        }
        CrewMember member = game.getMember(member_choice -1);
        int turns;
        if( member.getMovement() > 0){
            turns =  member.getMovement() + game.getMovementUpdate();
        }else{
            turns = 1;
        }
        int turn = 0;
        while (turns > 0) {
            turn++;
            List<Room> closeRooms = new ArrayList<>();
            Room old_room = game.getMemberRoom(member);
            if (member.getMovement() == 0) {
                for (Room r : game.getRooms()) {
                    if (!r.getSealed() && !r.equals(old_room)) {
                        closeRooms.add(r);
                    }
                }
            } else {
                closeRooms = game.getMemberCloseRooms(member);
            }
            System.out.println("O membro " + member.getName() + " pode ser movido mais "+turns+" vezes: ");
            int selectedRoom;
            boolean selected = false;
            do {
                for (Room r : closeRooms) {
                    if (!r.getSealed()) {
                        System.out.println("[" + r.getNumber() + "] - " + r.getName());
                    }
                }
                System.out.println("[0] - Sair");
                System.out.println("Escolha uma opção: ");
                selectedRoom = SC.nextInt();
                if(selectedRoom == 0){
                    selected = true;
                }
                for (Room r : closeRooms) {
                    if (!r.getSealed() && r.getNumber() == selectedRoom) {
                        selected = true;
                    }
                }
            }while (!selected);

            boolean noRoom = true;
            if(selectedRoom != 0) {
                while (noRoom) {
                    for (Room r : closeRooms) {
                        if (r.getNumber() == selectedRoom && !r.getSealed()) {
                            noRoom = false;
                        }
                    }
                    if (noRoom) {
                        System.out.println("Insira uma opção válida:");
                        selectedRoom = SC.nextInt();
                    }
                }
                Room newRoom = game.getRoomByNumber(selectedRoom);
                game.moveMember(member,old_room,newRoom,turn);
                System.out.println("O membro " + member.getName() + " foi movido para a sala " + newRoom.getName());
                turns--;
            }else{
                turns = 0;
                game.moveMember(null,null,null,turns);
            }
        }
    }
    private void uiBlowDisperser() {
        Scanner SC = new Scanner(System.in);
        System.out.println("Selecione uma sala para detonar um Particle Disperser:");
        int choice;
        boolean exists = false;
        do {
            for (Room room : game.getRooms()) {
                if (room.hasDispersers() && !room.getSealed()) {
                    System.out.println("[" + room.getNumber() + "] - " + room.getName());
                }
            }
            System.out.println("[0] - Sair");
            System.out.println("Escolha uma opção: ");
            while (!SC.hasNextInt()) {
                System.out.println("Insira um valor válido !");
                SC.next();
            }
            choice = SC.nextInt();
            if(choice== 0){
                exists = true;
            }else {
                for (Room room : game.getRooms()) {
                    if (room.hasDispersers() && !room.getSealed()) {
                        if (choice == room.getNumber()) {
                            exists = true;
                        }
                    }
                }
            }
        }while (!exists);
        if(choice!= 0) {
            Room room = game.getRoomByNumber(choice);
            game.useDisperser(room);
            if (game.getHealth() > 0) {
                System.out.println("Todos os Aliens na sala " + room.getName() + " foram evaporados!");
            } else {
                System.out.println("Um membro da tripulação foi detonado juntamente com os aliens!");
            }
        }else{
            game.useDisperser(null);
        }
    }
    private void uiSelectRoomToSeal() {
        Scanner SC = new Scanner(System.in);
        for (Room r:game.getRooms()) {
            if(!r.getSealed() && r.getNumber() != 1 && r.getNumber() != 2 && r.getNumber() != 5 && r.getNumber() != 6 && r.getNumber() != 8 && r.getNumber() != 10 && r.getAliensSize() == 0 && r.getMemberInRoom() == 0) {
                System.out.println("[" + r.getNumber() + "] - " + r.getName());
            }
        }
        System.out.println("Selecione uma sala: ");
        int room_to_seal = -1;
        boolean isValid = false;
        while (!isValid) {
            room_to_seal = SC.nextInt();
            for (Room r : game.getRooms()) {
                if(!r.getSealed() && r.getNumber() != 1 && r.getNumber() != 2 && r.getNumber() != 5 && r.getNumber() != 6 && r.getNumber() != 10 && r.getAliensSize() == 0 && r.getMemberInRoom() == 0 && r.getNumber() == room_to_seal) {
                    isValid = true;
                }
            }
            if(!isValid) {
                System.out.println("Insira uma opção válida:");
            }
        }
        Room room = game.getRoomByNumber(room_to_seal);

        game.setRoomSealed(room);
        System.out.println("A Sala "+room.getName() +" está agora fechada até ao fim do jogo!");
    }
    private void uiAttackAliens() {
        Scanner SC = new Scanner(System.in);
        System.out.println("Selecione um membro da tripulação para atacar!");
        int i = 1;
        for (CrewMember member: game.getMembers()) {
            if((game.getAliensInMemberRoom(member) > 0)) {
                System.out.println("[" + i + "] - " + member.getName());
            }
            i++;
        }
        System.out.println("[0] - Sair");
        System.out.println("Escolha uma opção: ");
        while (!SC.hasNextInt()) {
            System.out.println("Insira um valor válido!");
            SC.next();
        }
        int member_number;
        CrewMember member = null;
        while (true) {
            member_number = SC.nextInt();
            if(member_number >= 0 && member_number <= game.getMembersSize() +1) {
                member = game.getMember(member_number - 1);
            }
            if (member != null) {
                break;
            }
            System.out.println("Insira uma opção válida:");
        }
        if(member instanceof ScienceOfficer && game.scienceOfficerSpecial()){
            Room actual_room = game.getMemberRoom(member);
            System.out.println("Selecione uma sala!");
            if(actual_room.getAliensSize() > 0){
                System.out.println("[" + actual_room.getNumber() + "] - " + actual_room.getName()+" (Sala atual)");
            }
            for (Room room : actual_room.getCloseRooms()) {
                if (room.getAliensSize() > 0) {
                    System.out.println("[" + room.getNumber() + "] - " + room.getName());
                }
            }
            System.out.println("Escolha uma opção: ");
            while (!SC.hasNextInt()) {
                System.out.println("Insira um valor válido!");
                SC.next();
            }
            int selected_room = SC.nextInt();
            boolean isRoom = false;
            Room choosen_room = actual_room;
            while (!isRoom) {
                if(selected_room != actual_room.getNumber()) {
                    for (Room room : game.getMemberRoom(member).getCloseRooms()) {
                        if (room.getAliensSize() > 0 && room.getNumber() == selected_room) {
                            choosen_room = room;
                            isRoom = true;
                        }
                    }
                }
                else {
                    isRoom = true;
                }
                if(!isRoom){
                    System.out.println("Escolha uma opção válida!");
                    selected_room = SC.nextInt();
                }
            }
            int amountAliens = choosen_room.getAliensSize();
            if (amountAliens > 0) {
                int dices = member.getAttack() + game.getAttackDiceUpdate();
                int total = 0;
                System.out.println("Pretende definir um valor do ataque? (s/N)");
                Scanner SCawser = new Scanner(System.in);
                String awser;
                boolean isawser = false, attack = false;
                while (!isawser) {
                    awser = SCawser.nextLine().toUpperCase().trim();
                    if (awser.length() == 0) {
                        isawser = true;
                    } else if (awser.charAt(0) == 'S') {
                        isawser = attack = true;

                    } else if (awser.charAt(0) == 'N') {
                        isawser = true;
                    }
                    if (!isawser) {
                        System.out.println("Insira uma opção válida!");
                    }
                }
                if (attack) {
                    System.out.println("Insira um valor de 1 a 6 :");
                    while (!SC.hasNextInt()) {
                        System.out.println("Insira um valor válido!");
                        SC.next();
                    }
                    boolean notSet = true;
                    while (notSet) {
                        total = SC.nextInt();
                        if (total > 0 && total < 7) {
                            notSet = false;
                        }
                        if (notSet) {
                            System.out.println("Insira um valor válido :");
                        }
                    }
                } else {
                    for (int a = 0; a < dices; a++) {
                        total += game.randomWithRange(6);
                    }
                }
                game.attack(member,choosen_room,total,dices,amountAliens);
                System.out.println(game.getOutput());
            } else {
                System.out.println("Não existem aliens nesta sala!");
            }
        }else {
            Room room = game.getMemberRoom(member);
            int amountAliens = room.getAliensSize();
            if (amountAliens > 0) {
                int dices = member.getAttack() + game.getAttackDiceUpdate();
                int total = 0;
                System.out.println("Pretende definir um valor do ataque? (s/N)");
                Scanner SCawser = new Scanner(System.in);
                String awser;
                boolean isawser = false, attack = false;
                while (!isawser) {
                    awser = SCawser.nextLine().toUpperCase().trim();
                    if (awser.length() == 0) {
                        isawser = true;
                    } else if (awser.charAt(0) == 'S') {
                        isawser = attack = true;

                    } else if (awser.charAt(0) == 'N') {
                        isawser = true;
                    }
                    if (!isawser) {
                        System.out.println("Insira uma opção válida!");
                    }
                }
                if (attack) {
                    System.out.println("Insira um valor de 1 a 6 :");
                    while (!SC.hasNextInt()) {
                        System.out.println("Insira um valor válido!");
                        SC.next();
                    }
                    boolean notSet = true;
                    while (notSet) {
                        total = SC.nextInt();
                        if (total > 0 && total < 7) {
                            notSet = false;
                        }
                        if (notSet) {
                            System.out.println("Insira um valor válido :");
                        }
                    }
                } else {
                    for (int a = 0; a < dices; a++) {
                        total += game.randomWithRange(6);
                    }
                }
                game.attack(member,game.getMemberRoom(member),total,dices,amountAliens);
                System.out.println(game.getOutput());
            } else {
                System.out.println("Não existem aliens nesta sala!");
            }
        }
    }
    private void uiSetTrap() {
        Scanner SC = new Scanner(System.in);
        if(game.getDetonatorUpdate() + game.getDisperserUpdate() > 0) {
            AlienTraps trap = null;
            System.out.println("Selecione uma armadilha:");
            if(game.getDetonatorUpdate() > 0) {
                System.out.println("[1] - " + new OrganicDetonator().getName()+" (Disponível: "+game.getDetonatorUpdate()+")");
            }
            if(game.getDisperserUpdate() > 0) {
                System.out.println("[2] - " + new ParticleDisperser().getName()+" (Disponível: "+game.getDisperserUpdate()+")");
            }
            System.out.println("[0] - Sair");
            System.out.println("Escolha uma opção: ");
            while (!SC.hasNextInt()) {
                System.out.println("Insira um valor entre 0 e 2!");
                SC.next();
            }
            int choice = SC.nextInt();
            while (true) {
                if(game.getDetonatorUpdate() > 0 && choice == 1){
                    trap = new OrganicDetonator();
                    break;
                }
                if(game.getDisperserUpdate() > 0 && choice == 2){
                    trap = new ParticleDisperser();
                    break;
                }
                if(choice == 0){
                    break;
                }
                System.out.println("Insira uma opção válida!");
                choice = SC.nextInt();
            }
            System.out.println("Selecione uma sala:");
            for (Room r : game.getRooms()) {
                if (!r.getSealed() && r.getTraps() == 0) {
                    System.out.println("[" + r.getNumber() + "] - " + r.getName());
                }
            }
            System.out.println("Escolha uma opção: ");
            int room_numb = SC.nextInt();
            boolean noRoom = true;
            while (noRoom) {
                for (Room r : game.getRooms()) {
                    if (!r.getSealed() && r.getTraps() == 0) {
                        noRoom = false;
                    }
                }
                if (noRoom) {
                    System.out.println("Insira uma opção válida:");
                    room_numb = SC.nextInt();
                }
            }
            Room room =  game.getRoomByNumber(room_numb);
            if(trap != null) {
                game.spendTrap(room, trap);
                System.out.println("O " + trap.getName() + " foi colocado na sala " + room.getName() + "!");
            }else{
                System.out.println("Ocorreu um erro ao selecionar a armadilha! Tente novamente!");
            }
        }else{
            System.out.println("Não possui armadilhas para serem colocadas!");
        }
    }
    private void uiLooseGame() {
        if(game.getHull() <= 0 && game.getHealth() <= 0){
            System.out.println("Ohhh!!! Infelizmente não consegiu terminar o jogo, a sua nave sofreu um acidente e os seus tripulantes morreram!");
        }else if (game.getHealth() <= 0){
            System.out.println("Ohhh!!! Infelizmente não consegiu terminar o jogo, não existem mais tripulantes vivos!");
        }else if(game.getHull() <= 0){
            System.out.println("Ohhh!!! Infelizmente não consegiu terminar o jogo, a sua nave está inoperacional!");
        }else{
            System.out.println("Ohhh!!! Infelizmente não consegiu terminar o jogo!");
        }
        System.out.println("Melhor sorte para a proxima!");
        System.out.println("O que pretende fazer?");
        System.out.println("[1] - Jogar novamente");
        System.out.println("[0] - Sair");
        System.out.println("Escolha uma opção:");
        Scanner SC = new Scanner(System.in);
        while (!SC.hasNextInt()) {
            System.out.println("Insira um valor entre 0 e 1!");
            SC.next();
        }
        int option = SC.nextInt();
        if(option == 1){
            game.restartGame();
        }else {
            quit = true;
        }
    }
    private void uiWinGame() {
        System.out.println("PARABÉNS!!!!! Consegiu chegar a terra com os seus tripulantes!");
        System.out.println("O que pretende fazer?");
        System.out.println("[1] - Jogar novamente");
        System.out.println("[0] - Sair");
        System.out.println("Escolha uma opção:");
        Scanner SC = new Scanner(System.in);
        while (!SC.hasNextInt()) {
            System.out.println("Insira um valor entre 0 e 1!");
            SC.next();
        }
        int option = SC.nextInt();
        if(option == 1){
            game.restartGame();
        }else {
            quit = true;
        }
    }

    private void redShirtSpecial(){
        boolean exists = false;
        for (CrewMember m:game.getMembers()) {
            if(m instanceof RedShirt){
                exists = true;
            }
        }
        if (exists){
            System.out.println("Deseja abdicar do RedShirt por 5 de vida? (s/N)");
            Scanner SC = new Scanner(System.in);
            String awser = "";
            boolean isawser = false;
            while (!isawser){
                awser = SC.nextLine().toUpperCase().trim();
                if (awser.length() == 0){
                    isawser = true;
                } else if(awser.charAt(0) == 'S' || awser.charAt(0) == 'N'){
                    isawser = true;
                }
                if (!isawser){
                    System.out.println("Insira uma opção válida!");
                }
            }
            if(awser.length() != 0 && awser.charAt(0) == 'S'){
                if(game.killRedShirt()){
                    System.out.println("O membro RedShirt foi removido, a vida dos seus tripulantes é agora "+game.getHealth());
                }else{
                    System.out.println("O tripulante RedShirt continua em jogo!");
                }
            }else{
                System.out.println("O tripulante RedShirt continua em jogo!");
            }
        }
    }

    private void updatePlayer(){
        System.out.println("Vida dos Tripulantes: "+game.getHealth());
        System.out.println("Vida da nave: "+game.getHull());
        System.out.println("Inspiration Points: "+game.getIP());
        System.out.println("Particle Dispersers: "+game.getDisperserUpdate());
        System.out.println("Organic Detonators: "+game.getDetonatorUpdate());
        System.out.println("Fechar salas: "+game.getSealedRoomUpdate());
        System.out.println("Movimento: +"+game.getMovementUpdate());
        System.out.println("Attack Dices: +"+game.getAttackDiceUpdate());
        System.out.println("Result Attack: +"+game.getResultAttackUpdate());
        for (CrewMember member:game.getMembers()) {
            System.out.println("O membro "+member.getName()+ " está na sala "+game.getMemberRoomName(member));
        }
        for (Room room:game.getRooms()) {
            if(room.getAliensSize() > 0){
                System.out.println("Existem "+room.getAliensSize()+ " aliens na sala "+room.getName());
            }
        }
    }
    private void makeEnters(){
        for (int i = 0; i < 3; i++){
            System.out.println();
        }
    }
}
