package userInterface.text;

import gameLogic.Logic;

public class Console {

    public static void main(String[] args) {
        TextInterface game = new TextInterface(new Logic());
        game.run();
    }
}
