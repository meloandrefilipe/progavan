package userInterface.graphics;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;

class NotificationBox {

    static void display(String message) {
        Toolkit.getDefaultToolkit().beep();
        final double[] xOffset = new double[1];
        final double[] yOffset = new double[1];
        Stage window = new Stage();
        window.setWidth(800);
        window.setHeight(200);
        window.initModality(Modality.APPLICATION_MODAL);
        window.initStyle(StageStyle.UNDECORATED);
        window.getIcons().add(new Image("img/deIcon.png"));
        Label label = new Label(message);
        label.setAlignment(Pos.CENTER);
        HBox.setHgrow(label, Priority.ALWAYS);
        Button btnYes = new Button("Ok");
        btnYes.getStyleClass().add("btn-yes");
        btnYes.setOnAction(event -> {
            window.close();
        });
        HBox buttonsBox = new HBox(25);
        buttonsBox.getChildren().add(btnYes);
        buttonsBox.setAlignment(Pos.CENTER);

        VBox layout = new VBox(20);
        layout.getChildren().addAll(label,buttonsBox);
        layout.setAlignment(Pos.CENTER);
        layout.setMaxWidth(Double.MAX_VALUE);
        Scene popup = new Scene(layout);
        popup.getStylesheets().add("css/confirmBox.css");

        popup.setOnMousePressed(event -> {
            xOffset[0] = window.getX() - event.getScreenX();
            yOffset[0] = window.getY() - event.getScreenY();
        });

        popup.setOnMouseDragged(event ->  {
            window.setX(event.getScreenX() + xOffset[0]);
            window.setY(event.getScreenY() + yOffset[0]);
        });

        window.setScene(popup);
        window.showAndWait();
    }
}
