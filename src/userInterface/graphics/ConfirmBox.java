package userInterface.graphics;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;

class ConfirmBox {
    private static boolean awser;

    static boolean display(String title, String message) {
        Toolkit.getDefaultToolkit().beep();
        final double[] xOffset = new double[1];
        final double[] yOffset = new double[1];
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.initStyle(StageStyle.UNDECORATED);
        window.setTitle(title);
        window.getIcons().add(new Image("img/deIcon.png"));
        Label label = new Label(message);

        Button btnYes = new Button("Sim");
        btnYes.getStyleClass().add("btn-yes");
        Button btnNo = new Button("Não");
        btnNo.getStyleClass().add("btn-no");
        btnYes.setOnAction(event -> {
            awser = true;
            window.close();
        });
        btnNo.setOnAction(event -> {
            awser = false;
            window.close();
        });
        HBox buttonsBox = new HBox(25);
        buttonsBox.getChildren().addAll(btnYes,btnNo);
        buttonsBox.setAlignment(Pos.CENTER);

        VBox layout = new VBox(20);
        layout.getChildren().addAll(label,buttonsBox);
        layout.setAlignment(Pos.CENTER);
        Scene popup = new Scene(layout);
        popup.getStylesheets().add("css/confirmBox.css");

        popup.setOnMousePressed(event -> {
            xOffset[0] = window.getX() - event.getScreenX();
            yOffset[0] = window.getY() - event.getScreenY();
        });

        popup.setOnMouseDragged(event ->  {
            window.setX(event.getScreenX() + xOffset[0]);
            window.setY(event.getScreenY() + yOffset[0]);
        });


        window.setScene(popup);
        window.showAndWait();
        return awser;
    }
}
