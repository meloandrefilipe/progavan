package userInterface.graphics;

import gameLogic.Logic;
import gameLogic.ObservableGame;
import gameLogic.Room;
import gameLogic.members.*;
import gameLogic.states.*;
import gameLogic.tracker.TrackerPosition;
import gameLogic.traps.AlienTraps;
import gameLogic.traps.OrganicDetonator;
import gameLogic.traps.ParticleDisperser;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class GraphicInterface extends Application implements PropertyChangeListener {
    private Stage window;
    private Scene mainMenu;
    private double xOffset;
    private double yOffset;
    private ObservableGame game = new ObservableGame(new Logic());
    private BorderPane layoutGame;
    private int change_rooms = 0;
    private int turn = 0;
    private List<CrewMember> selectedMember = new ArrayList<>();
    private GraphicsDevice monitor = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private int SCREENWIDTH = monitor.getDisplayMode().getWidth();
    private int SCREENHEIGHT = monitor.getDisplayMode().getHeight();
    private int REFRESH = 0;

    public static void main(String[] args) {
        launch(args);
    }



    @Override
    public void start(Stage window){
        this.window = window;
        window.initStyle(StageStyle.UNDECORATED);
        window.setTitle("Destination Earth");
        window.getIcons().add(new Image("img/deIcon.png"));
        window.setResizable(false);
        window.setWidth(SCREENWIDTH / 2);
        window.setHeight(SCREENHEIGHT / 2);

        VBox menuList = new VBox(25);

        Button btn_start = new Button("Iniciar Jogo");
        btn_start.getStyleClass().add("btn-start");
        btn_start.setOnAction(e -> {
            game = new ObservableGame(new Logic());
            layoutGame = new BorderPane();
            setup();
        });

        Button btn_load = new Button("Carregar Jogo");
        btn_load.getStyleClass().add("btn-load");
        btn_load.setOnAction(e -> {
            try {
                layoutGame = new BorderPane();
                game.loadGame();
                NotificationBox.display("Jogo carregado com sucesso!");
                play();
            } catch (IOException | ClassNotFoundException error) {
                NotificationBox.display("Erro ao carregar o ficheiro!");
                error.printStackTrace();
            }
        });

        Button btn_delete = new Button("Apagar Jogo");
        btn_delete.getStyleClass().add("btn-delete");
        btn_delete.setOnAction(event -> {
            if(game.fileDelete()){
                NotificationBox.display("Jogo apagado com sucesso!");
            }else {
                NotificationBox.display("Não foi possivel apagar o ficheiro do jogo!");
            }
        });

        Button btn_exit = new Button("Sair");
        btn_exit.getStyleClass().add("btn-exit");
        btn_exit.setOnAction(event -> {
            if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                window.close();
            }
        });

        menuList.getChildren().add(btn_start);


        if (game.fileExists()) {
            menuList.getChildren().addAll(btn_load, btn_delete);
        }
        menuList.getChildren().add(btn_exit);
        menuList.setAlignment(Pos.CENTER);


        StackPane layoutMenu = new StackPane();
        layoutMenu.getChildren().addAll(menuList);

        mainMenu = new Scene(layoutMenu);
        mainMenu.getStylesheets().add("css/mainMenu.css");

        mainMenu.setOnMousePressed(event -> {
            xOffset = window.getX() - event.getScreenX();
            yOffset = window.getY() - event.getScreenY();
        });

        mainMenu.setOnMouseDragged(event -> {
            window.setX(event.getScreenX() + xOffset);
            window.setY(event.getScreenY() + yOffset);
        });

        mainMenu.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                    window.close();
                }
            }
        });

        window.setScene(mainMenu);
        window.show();
    }

    private void crewStore() {
        BorderPane Layout = new BorderPane();
        Layout.getStyleClass().add("stores");


        VBox Header = new VBox(10);
        Header.setAlignment(Pos.CENTER);
        Header.fillWidthProperty();
        Header.getStyleClass().add("header");
        Label Title = new Label("Crew Phase");
        Title.getStyleClass().add("title");
        Title.setAlignment(Pos.CENTER);

        Header.getChildren().add(Title);

        Layout.setTop(Header);


        VBox List = new VBox(10);
        List.setAlignment(Pos.TOP_CENTER);
        List.fillWidthProperty();
        List.getStyleClass().add("list");
        List.fillWidthProperty();
        List.setPrefHeight(Region.USE_COMPUTED_SIZE);

        Button moveMember = new Button("Mover Tripulante");
        VBox moveMemberRoomList = new VBox(10);
        moveMemberRoomList.setAlignment(Pos.TOP_CENTER);
        moveMemberRoomList.getStyleClass().add("listMove");
        moveMemberRoomList.fillWidthProperty();
        moveMemberRoomList.setPrefHeight(Region.USE_COMPUTED_SIZE);
        Button attackAliens = new Button("Atacar Aliens");
        VBox attackRoomList = new VBox(10);
        attackRoomList.setAlignment(Pos.TOP_CENTER);
        attackRoomList.getStyleClass().add("listMove");
        attackRoomList.fillWidthProperty();
        attackRoomList.setPrefHeight(Region.USE_COMPUTED_SIZE);
        Button buyHealth = new Button("Curar Tripulação");
        Button buyHull = new Button("Reparar a nave");
        Button setTrap = new Button("Colocar Armadilha");
        VBox setTrapRoomList = new VBox(10);
        setTrapRoomList.setAlignment(Pos.TOP_CENTER);
        setTrapRoomList.fillWidthProperty();
        setTrapRoomList.getStyleClass().add("list");
        setTrapRoomList.fillWidthProperty();
        setTrapRoomList.setPrefHeight(Region.USE_COMPUTED_SIZE);
        Button useDisperser = new Button("Detonar Particle Disperser");
        VBox useDisperserRoomList = new VBox(10);
        useDisperserRoomList.setAlignment(Pos.TOP_CENTER);
        useDisperserRoomList.fillWidthProperty();
        useDisperserRoomList.getStyleClass().add("list");
        useDisperserRoomList.fillWidthProperty();
        useDisperserRoomList.setPrefHeight(Region.USE_COMPUTED_SIZE);
        Button useSealToken = new Button("Fechar Sala");
        VBox sealTokenRoomList = new VBox(10);
        sealTokenRoomList.setAlignment(Pos.TOP_CENTER);
        sealTokenRoomList.fillWidthProperty();
        sealTokenRoomList.getStyleClass().add("list");
        sealTokenRoomList.fillWidthProperty();
        sealTokenRoomList.setPrefHeight(Region.USE_COMPUTED_SIZE);

        buyHealth.getStyleClass().add("listButton");
        buyHull.getStyleClass().add("listButton");
        moveMember.getStyleClass().add("listButton");
        attackAliens.getStyleClass().add("listButton");
        setTrap.getStyleClass().add("listButton");
        useDisperser.getStyleClass().add("listButton");
        useSealToken.getStyleClass().add("listButton");

        buyHealth.setAlignment(Pos.CENTER);
        buyHull.setAlignment(Pos.CENTER);
        moveMember.setAlignment(Pos.CENTER);
        attackAliens.setAlignment(Pos.CENTER);
        setTrap.setAlignment(Pos.CENTER);
        useDisperser.setAlignment(Pos.CENTER);
        useSealToken.setAlignment(Pos.CENTER);
        int total_ap = game.getAP();
        List.getChildren().clear();
        List.getChildren().addAll(moveMember, moveMemberRoomList);
        if (game.isAliensAndMembersSameRoom()) {
            List.getChildren().addAll(attackAliens, attackRoomList);
        }
        if (game.isThereAnyDoctor() && game.getHealth() < 12) {
            List.getChildren().add(buyHealth);
        }
        if (game.isThereAnyEngineer() && game.getHull() < 12) {
            List.getChildren().add(buyHull);
        }
        if (game.getDetonatorUpdate() > 0 || game.getDisperserUpdate() > 0) {
            List.getChildren().addAll(setTrap, setTrapRoomList);
        }
        if (game.anyDispersers()) {
            List.getChildren().addAll(useDisperser, useDisperserRoomList);
        }
        if (game.getSealedRoomUpdate() > 0) {
            List.getChildren().addAll(useSealToken, sealTokenRoomList);
        }

        buyHealth.setOnAction(event -> {
            game.spendActionPoints(3);
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!buyHealth.getStyleClass().contains("btnActive")) {
                useDisperser.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().add("btnActive");
                if (game.getHealth() < 12) {
                    NotificationBox.display("A vida dos seus tripulantes agora é: " + game.getHealth());
                } else {
                    NotificationBox.display("A vida dos seus tripulantes já está no maximo!");
                }
            }else{
                buyHealth.getStyleClass().remove("btnActive");
            }
            updateGame();
        });
        buyHull.setOnAction(event -> {
            game.spendActionPoints(4);
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!buyHull.getStyleClass().contains("btnActive")) {
                useDisperser.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().add("btnActive");
                if (game.getHull() < 12) {
                    NotificationBox.display("A vida da sua nave agora é: " + game.getHull());
                } else {
                    NotificationBox.display("A vida da sua nave já está no maximo!");
                }
            }else{
                buyHull.getStyleClass().remove("btnActive");
            }
            updateGame();
        });
        moveMember.setOnAction(event -> {
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!moveMember.getStyleClass().contains("btnActive")) {
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().remove("btnActive");
                useDisperser.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().add("btnActive");

                moveMemberRoomList.getChildren().clear();
                VBox BoxMove = new VBox(10);
                BoxMove.getChildren().clear();
                BoxMove.fillWidthProperty();
                BoxMove.setPrefHeight(Region.USE_COMPUTED_SIZE);
                HBox membersBox = new HBox(10);
                membersBox.setAlignment(Pos.CENTER);
                membersBox.getChildren().clear();
                membersBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
                VBox CloseRooms = new VBox(10);
                CloseRooms.getChildren().clear();
                CloseRooms.fillWidthProperty();
                CloseRooms.setPrefHeight(Region.USE_COMPUTED_SIZE);
                for (CrewMember member : game.getMembers()) {
                    Button btnMember = new Button(member.getName());
                    HBox.setHgrow(btnMember, Priority.ALWAYS);
                    btnMember.setMaxWidth(Double.MAX_VALUE);
                    btnMember.getStyleClass().add("btnMemberMove");
                    if(game.getAP() > 0) {
                        membersBox.getChildren().add(btnMember);
                    }else if(game.getAP() == 0 && change_rooms > 0 && member.equals(selectedMember.get(0))){
                        membersBox.getChildren().add(btnMember);
                    }
                    btnMember.setOnAction(event1 -> {
                        if(selectedMember.isEmpty()){
                            selectedMember.add(member);
                            turn = 0;
                            if (member.getMovement() > 0) {
                                change_rooms = member.getMovement() + game.getMovementUpdate();
                            } else {
                                change_rooms = 1;
                            }
                        }else{
                            if(!member.equals(selectedMember.get(0))){
                                selectedMember.clear();
                                selectedMember.add(member);
                                turn = 0;
                                if (member.getMovement() > 0) {
                                    change_rooms = member.getMovement() + game.getMovementUpdate();
                                } else {
                                    change_rooms = 1;
                                }
                            }else if(member.equals(selectedMember.get(0)) && change_rooms <= 0){
                                turn = 0;
                                if (member.getMovement() > 0) {
                                    change_rooms = member.getMovement() + game.getMovementUpdate();
                                } else {
                                    change_rooms = 1;
                                }
                            }
                        }
                        CloseRooms.getChildren().clear();
                        canChangeRoom(member,CloseRooms);
                    });
                }
                BoxMove.getChildren().add(membersBox);
                BoxMove.getChildren().add(CloseRooms);
                moveMemberRoomList.getChildren().add(BoxMove);
            }else{
                moveMember.getStyleClass().remove("btnActive");
                moveMemberRoomList.getChildren().clear();
            }
        });
        attackAliens.setOnAction(event -> {
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!attackAliens.getStyleClass().contains("btnActive")) {
                setTrap.getStyleClass().remove("btnActive");
                useDisperser.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().add("btnActive");

                attackRoomList.getChildren().clear();
                VBox BoxAttack = new VBox(10);
                BoxAttack.getChildren().clear();
                BoxAttack.fillWidthProperty();
                BoxAttack.setPrefHeight(Region.USE_COMPUTED_SIZE);
                HBox membersBox = new HBox(10);
                membersBox.getChildren().clear();
                membersBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
                VBox CloseRooms = new VBox(10);
                CloseRooms.getChildren().clear();
                CloseRooms.fillWidthProperty();
                CloseRooms.setPrefHeight(Region.USE_COMPUTED_SIZE);
                for (CrewMember member : game.getMembers()) {
                    Button btnMember = new Button(member.getName());
                    HBox.setHgrow(btnMember, Priority.ALWAYS);
                    btnMember.setMaxWidth(Double.MAX_VALUE);
                    btnMember.getStyleClass().add("btnMemberMove");
                    if(game.getAliensInMemberRoom(member) > 0) {
                        membersBox.getChildren().add(btnMember);
                    }
                    btnMember.setOnAction(event1 -> {
                        Room member_room = game.getMemberRoom(member);
                        Button actualRoom = new Button(member_room.getName());
                        actualRoom.getStyleClass().add("btnSelectRoom");
                        HBox.setHgrow(actualRoom, Priority.ALWAYS);
                        actualRoom.setMaxWidth(Double.MAX_VALUE);
                        actualRoom.getStyleClass().add("btnSelectRoom");
                        CloseRooms.getChildren().add(actualRoom);
                        actualRoom.setOnAction(event2 -> {
                            game.spendActionPoints(2);
                            int dices = member.getAttack() + game.getAttackDiceUpdate();
                            int attack_value = 0;
                            for (int i = 0; i <dices ; i++) {
                                attack_value += game.randomWithRange(6);
                            }
                            game.attack(member,member_room,attack_value,dices,member_room.getAliensSize());
                            NotificationBox.display(game.getOutput());
                            updateGame();
                        });
                        if(member instanceof ScienceOfficer && game.scienceOfficerSpecial()){
                            for (Room room: game.getMemberCloseRooms(member)) {
                                if(room.getAliensSize() > 0){
                                    Button btnRoom = new Button(room.getName());
                                    btnRoom.getStyleClass().add("btnSelectRoom");
                                    HBox.setHgrow(btnRoom, Priority.ALWAYS);
                                    btnRoom.setMaxWidth(Double.MAX_VALUE);
                                    btnRoom.getStyleClass().add("btnSelectRoom");
                                    CloseRooms.getChildren().add(btnRoom);
                                    btnRoom.setOnAction(event2 -> {
                                        game.spendActionPoints(2);
                                        int dices = member.getAttack() + game.getAttackDiceUpdate();
                                        int attack_value = 0;
                                        for (int i = 0; i <dices ; i++) {
                                            attack_value += game.randomWithRange(6);
                                        }
                                        game.attack(member,room,attack_value,dices,room.getAliensSize());
                                        NotificationBox.display(game.getOutput());
                                        updateGame();
                                    });
                                }
                            }
                        }
                    });
                }
                BoxAttack.getChildren().add(membersBox);
                BoxAttack.getChildren().add(CloseRooms);
                attackRoomList.getChildren().add(BoxAttack);
            }else{
                attackAliens.getStyleClass().remove("btnActive");
                attackRoomList.getChildren().clear();
            }
        });
        setTrap.setOnAction(event -> {
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!setTrap.getStyleClass().contains("btnActive")){
                useDisperser.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().add("btnActive");
                VBox BoxSetTrap = new VBox(10);
                BoxSetTrap.getChildren().clear();
                BoxSetTrap.fillWidthProperty();
                BoxSetTrap.setPrefHeight(Region.USE_COMPUTED_SIZE);
                HBox trapsBox = new HBox(10);
                trapsBox.getChildren().clear();
                trapsBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
                Button detonator = new Button("Organic Detonator");
                HBox.setHgrow(detonator, Priority.ALWAYS);
                detonator.setMaxWidth(Double.MAX_VALUE);
                detonator.getStyleClass().add("btnMemberMove");
                if(game.getDetonatorUpdate() > 0) {
                    trapsBox.getChildren().add(detonator);
                }
                Button disperser = new Button("Particle Disperser");
                HBox.setHgrow(disperser, Priority.ALWAYS);
                disperser.setMaxWidth(Double.MAX_VALUE);
                disperser.getStyleClass().add("btnMemberMove");
                if(game.getDisperserUpdate() > 0) {
                    trapsBox.getChildren().add(disperser);
                }
                VBox setTrapRooms = new VBox(10);
                setTrapRooms.getChildren().clear();
                setTrapRooms.fillWidthProperty();
                setTrapRooms.setPrefHeight(Region.USE_COMPUTED_SIZE);

                detonator.setOnAction(event1 -> {
                    setTrapRooms.getChildren().clear();
                    for (Room room:game.getRooms()) {
                        if(!room.getSealed() && room.getTraps() <= 0){
                            Button btnRoom = new Button(room.getName());
                            btnRoom.getStyleClass().add("btnSelectRoom");
                            HBox.setHgrow(btnRoom, Priority.ALWAYS);
                            btnRoom.setMaxWidth(Double.MAX_VALUE);
                            setTrapRooms.getChildren().add(btnRoom);
                            btnRoom.setOnAction(event2 ->{
                                game.spendActionPoints(5);
                                AlienTraps trap = new OrganicDetonator();
                                game.spendTrap(room, trap);
                                NotificationBox.display("Foi colocado um "+trap.getName()+" na sala "+room.getName());
                                updateGame();
                            });
                        }
                    }
                });
                disperser.setOnAction(event1 -> {
                    setTrapRooms.getChildren().clear();
                    for (Room room:game.getRooms()) {
                        if(!room.getSealed() && room.getTraps() <= 0){
                            Button btnRoom = new Button(room.getName());
                            btnRoom.getStyleClass().add("btnSelectRoom");
                            HBox.setHgrow(btnRoom, Priority.ALWAYS);
                            btnRoom.setMaxWidth(Double.MAX_VALUE);
                            setTrapRooms.getChildren().add(btnRoom);
                            btnRoom.setOnAction(event2 ->{
                                game.spendActionPoints(5);
                                AlienTraps trap = new ParticleDisperser();
                                game.spendTrap(room, trap);
                                NotificationBox.display("Foi colocado um "+trap.getName()+" na sala "+room.getName());
                                updateGame();
                            });
                        }
                    }
                });



                BoxSetTrap.getChildren().addAll(trapsBox,setTrapRooms);
                setTrapRoomList.getChildren().add(BoxSetTrap);
            }else{
                setTrap.getStyleClass().remove("btnActive");
                setTrapRoomList.getChildren().clear();
            }
        });
        useDisperser.setOnAction(event -> {
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!useDisperser.getStyleClass().contains("btnActive")) {
                buyHull.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                useDisperser.getStyleClass().add("btnActive");

                for (Room room : game.getRooms()) {
                    if (!room.getSealed() && room.hasDispersers()) {
                        Button btnRoom = new Button(room.getName());
                        btnRoom.getStyleClass().add("btnSelectRoom");
                        HBox.setHgrow(btnRoom, Priority.ALWAYS);
                        btnRoom.setMaxWidth(Double.MAX_VALUE);
                        btnRoom.getStyleClass().add("btnSelectRoom");
                        useDisperserRoomList.getChildren().add(btnRoom);
                        btnRoom.setOnAction(event1 -> {
                            game.spendActionPoints(6);
                            game.useDisperser(room);
                            if (game.getHealth() > 0) {
                                NotificationBox.display("Todos os Aliens na sala " + room.getName() + " foram evaporados!");
                            } else {
                                NotificationBox.display("Um membro da tripulação foi detonado!");
                            }
                            updateGame();
                        });
                    }
                }
            }else{
                useDisperser.getStyleClass().remove("btnActive");
            }
        });
        useSealToken.setOnAction(event -> {
            moveMemberRoomList.getChildren().clear();
            attackRoomList.getChildren().clear();
            setTrapRoomList.getChildren().clear();
            useDisperserRoomList.getChildren().clear();
            sealTokenRoomList.getChildren().clear();
            if(!useSealToken.getStyleClass().contains("btnActive")) {
                useDisperser.getStyleClass().remove("btnActive");
                buyHull.getStyleClass().remove("btnActive");
                buyHealth.getStyleClass().remove("btnActive");
                moveMember.getStyleClass().remove("btnActive");
                attackAliens.getStyleClass().remove("btnActive");
                setTrap.getStyleClass().remove("btnActive");
                useSealToken.getStyleClass().add("btnActive");

                for (Room room : game.getRooms()) {
                    if (!room.getSealed() && room.getNumber() != 1 && room.getNumber() != 2 && room.getNumber() != 5 && room.getNumber() != 6 && room.getNumber() != 8 && room.getNumber() != 10 && room.getAliensSize() == 0 && room.getMemberInRoom() == 0) {
                        Button btnRoom = new Button(room.getName());
                        btnRoom.getStyleClass().add("btnSelectRoom");
                        HBox.setHgrow(btnRoom, Priority.ALWAYS);
                        btnRoom.setMaxWidth(Double.MAX_VALUE);
                        sealTokenRoomList.getChildren().add(btnRoom);
                        btnRoom.setOnAction(event1 -> {
                            game.spendActionPoints(7);
                            game.setRoomSealed(room);
                            NotificationBox.display("A Sala " + room.getName() + " está agora fechada até ao fim do jogo!");
                            updateGame();
                        });
                    }
                }
            }else{
                useSealToken.getStyleClass().remove("btnActive");
                sealTokenRoomList.getChildren().clear();
            }
        });

        Layout.setCenter(List);


        Label Counter = new Label("Tem ainda "+ total_ap +" action points disponiveis para usar");
        Counter.getStyleClass().add("counter");
        Counter.setAlignment(Pos.CENTER);
        HBox Footer2 = new HBox();
        Footer2.setAlignment(Pos.CENTER);
        Footer2.getChildren().add(Counter);
        Footer2.getStyleClass().add("footer");
        if(total_ap > 0) {
            Layout.setBottom(Footer2);
        }


        Layout.getStylesheets().add("css/store.css");
        if(game.getAP() > 0 || change_rooms > 0) {
            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setContent(Layout);
            scrollPane.setFitToHeight(true);
            scrollPane.setFitToWidth(true);
            layoutGame.setRight(scrollPane);
        }else{
            NotificationBox.display("Já gastou todos os seus Action Points!");
            layoutGame.setRight(null);
        }
    }

    private void restStore() {
        if(game.getIP() > 0) {
            int total_ip = game.getIP();

            BorderPane Layout = new BorderPane();
            Layout.getStyleClass().add("stores");


            VBox Header = new VBox(10);
            Header.setAlignment(Pos.CENTER);
            Header.fillWidthProperty();
            Header.getStyleClass().add("header");
            Label Title = new Label("Rest Phase");
            Title.getStyleClass().add("title");
            Title.setAlignment(Pos.CENTER);

            Header.getChildren().add(Title);

            Layout.setTop(Header);


            VBox List = new VBox(10);
            List.setAlignment(Pos.TOP_CENTER);
            List.fillWidthProperty();
            List.getStyleClass().add("list");
            List.fillWidthProperty();
            List.setPrefHeight(Region.USE_COMPUTED_SIZE);

            Button buyHealth = new Button("Curar Tripulação (Custo: 1)");
            Button buyHull = new Button("Reparar a nave (Custo: 1)");
            Button buyOrganic = new Button("Comprar um Organic Detonator (Custo: 2)");
            Button buyDisperser = new Button("Comprar um Particle Disperser (Custo: 5)");
            Button buyMovement = new Button("Comprar Movimento (Custo: 4)");
            Button buySealToken = new Button("Comprar chave de sala (Custo: 5)");
            Button buyAttack = new Button("Comprar força de ataque (Custo: 6)");
            Button buyAttackDice = new Button("Comprar dado (Custo: 6)");

            buyHealth.getStyleClass().add("listButton");
            buyHull.getStyleClass().add("listButton");
            buyOrganic.getStyleClass().add("listButton");
            buyDisperser.getStyleClass().add("listButton");
            buyMovement.getStyleClass().add("listButton");
            buySealToken.getStyleClass().add("listButton");
            buyAttack.getStyleClass().add("listButton");
            buyAttackDice.getStyleClass().add("listButton");

            buyHealth.setAlignment(Pos.CENTER);
            buyHull.setAlignment(Pos.CENTER);
            buyOrganic.setAlignment(Pos.CENTER);
            buyDisperser.setAlignment(Pos.CENTER);
            buyMovement.setAlignment(Pos.CENTER);
            buySealToken.setAlignment(Pos.CENTER);
            buyAttack.setAlignment(Pos.CENTER);
            buyAttackDice.setAlignment(Pos.CENTER);

            if(total_ip >= 1){
                List.getChildren().addAll(buyHealth,buyHull);
            }
            if(total_ip >=2){
                List.getChildren().addAll(buyOrganic);
            }
            if(total_ip >=4){
                List.getChildren().addAll(buyMovement);
            }
            if(total_ip >= 5){
                List.getChildren().addAll(buyDisperser,buySealToken);
            }
            if (total_ip >= 6){
                List.getChildren().addAll(buyAttack,buyAttackDice);
            }

            buyHealth.setOnAction(event -> {
                int health = game.getHealth();
                game.spendInspirationPoints(1);
                int points = game.getHealth() - health;
                if(points >1){
                    NotificationBox.display("Devido a existir um Engineer na tripulação, tem mais um ponto de vida da nave grátis!");
                }else if (points < 1){
                    NotificationBox.display("A vida dos tripulantes já está no maximo "+ game.getHealth()+ ", não foram gastos inspiration points!");

                }else {
                    NotificationBox.display("A vida dos membros foi aumentada para " + game.getHealth() + " e agora tem " + game.getIP() + " inspiration points.");
                }
                updateGame();
            });
            buyHull.setOnAction(event -> {
                int hull = game.getHull();
                game.spendInspirationPoints(2);
                int points = game.getHull() - hull;
                if(points >1){
                    NotificationBox.display("Devido a existir um Engineer na tripulação, tem mais um ponto de vida da nave grátis!");
                }else if (points < 1){
                    NotificationBox.display("A vida da nave já está no máximo "+game.getHull() +", não foram gastos inspiration points!");
                }else {
                    NotificationBox.display("A vida da nave foi aumentada para " + game.getHull() + " e agora tem " + game.getIP() + " inspiration points.");
                }
                updateGame();
            });
            buyOrganic.setOnAction(event -> {
                game.spendInspirationPoints(3);
                NotificationBox.display("Agora tem " + game.getDetonatorUpdate() + " organic detonators e " + game.getIP() + " inspiration points.");
                updateGame();
            });
            buyMovement.setOnAction(event -> {
                game.spendInspirationPoints(4);
                NotificationBox.display("Agora os membros podem mover-se mais " + game.getMovementUpdate() + " salas e tem " + game.getIP() + " inspiration points.");
                updateGame();
            });
            buyDisperser.setOnAction(event -> {
                game.spendInspirationPoints(5);
                NotificationBox.display("Agora tem " + game.getDisperserUpdate() + " particle disperser e " + game.getIP() + " inspiration points.");
                updateGame();
            });
            buySealToken.setOnAction(event -> {
                game.spendInspirationPoints(6);
                NotificationBox.display("Agora pode fechar " + game.getSealedRoomUpdate() + " salas e tem " + game.getIP() + " inspiration points.");
                updateGame();
            });
            buyAttack.setOnAction(event -> {
                game.spendInspirationPoints(7);
                NotificationBox.display("Agora são lançados " + game.getAttackDiceUpdate() + " dados a cada ataque e tem " + game.getIP() + " inspiration points.");
                updateGame();
            });
            buyAttackDice.setOnAction(event -> {
                game.spendInspirationPoints(8);
                NotificationBox.display("Agora faz um dano extra de " + game.getResultAttackUpdate() + " e tem " + game.getIP() + " inspiration points.");
                updateGame();
            });


            Layout.setCenter(List);

            Label Counter = new Label("Tem ainda "+ total_ip +" inspiration points disponiveis para usar");
            Counter.getStyleClass().add("counter");
            Counter.setAlignment(Pos.CENTER);
            HBox Footer = new HBox();
            Footer.setAlignment(Pos.CENTER);
            Footer.getChildren().add(Counter);
            Footer.getStyleClass().add("footer");

            Layout.setBottom(Footer);
            Layout.getStylesheets().add("css/store.css");

            if(game.getIP() > 0) {
                ScrollPane scrollPane = new ScrollPane();
                scrollPane.setContent(Layout);
                scrollPane.setFitToHeight(true);
                scrollPane.setFitToWidth(true);
                layoutGame.setRight(scrollPane);
            }else{
                NotificationBox.display("Já gastou todos os seus Inspiration Points!");
                layoutGame.setRight(null);
            }
        }else{
            layoutGame.setRight(null);
            NotificationBox.display("Não tem Inspiration Points disponíveis!");
            game.spendInspirationPoints(0);
            updateGame();
        }
    }

    private void updateGame() {
        game.addPropertyChangeListener(this);
        REFRESH = 0;
        game.updateGame();
    }

    private void refreshGame() {
        refreshBottom();
        refreshCenter();
        refreshLeft();
        refreshTop();
        refreshRight();
        GameState state = game.getState();
        if(state instanceof LooseGame){
            if(game.getHull() <= 0 && game.getHealth() <= 0){
                NotificationBox.display("Ohhh!!! Infelizmente não consegiu terminar o jogo, a sua nave sofreu um acidente e os seus tripulantes morreram!");
            }else if (game.getHealth() <= 0){
                NotificationBox.display("Ohhh!!! Infelizmente não consegiu terminar o jogo, não existem mais tripulantes vivos!");
            }else if(game.getHull() <= 0){
                NotificationBox.display("Ohhh!!! Infelizmente não consegiu terminar o jogo, a sua nave está inoperacional!");
            }else{
                NotificationBox.display("Ohhh!!! Infelizmente não consegiu terminar o jogo!");
            }
            window.setScene(mainMenu);
        }
        if(state instanceof WinGame){
            NotificationBox.display("PARABÉNS!!!!! Consegiu chegar a terra com os seus tripulantes!");
            window.setScene(mainMenu);
        }
    }

    private void refreshTop() {
        GridPane Top = new GridPane();
        Top.getStyleClass().add("topPanel");
        ColumnConstraints column;
        Top.setAlignment(Pos.CENTER);
        Top.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        Top.setVgap(2);
        Top.setHgap(2);

        HBox JourneyTrackerBox = new HBox(3);
        JourneyTrackerBox.setAlignment(Pos.CENTER);
        int t = 0;
        for (TrackerPosition position : game.getTrackerPositions()) {
            Label label = new Label(position.getName());
            label.getStyleClass().add("tackerLabels");
            if (t == game.getTrackerCurrent()) {
                label.getStyleClass().add("tackerLabelsActive");
            }
            label.setAlignment(Pos.CENTER);
            JourneyTrackerBox.getChildren().add(label);
            t++;
        }

        HBox FuncButtons = new HBox();
        FuncButtons.setAlignment(Pos.TOP_RIGHT);
        Button btnExit = new Button("X");
        btnExit.getStyleClass().add("btnExit");
        btnExit.setAlignment(Pos.CENTER);
        FuncButtons.getChildren().add(btnExit);
        FuncButtons.getStyleClass().add("funcButtons");

        btnExit.setOnAction(event -> {
            if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                window.setScene(mainMenu);
                window.setMaximized(false);
            }
        });


        column = new ColumnConstraints();
        column.fillWidthProperty();
        column.setPercentWidth(10);
        Top.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.fillWidthProperty();
        column.setPercentWidth(80);
        Top.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.fillWidthProperty();
        column.setPercentWidth(10);
        Top.getColumnConstraints().add(column);

        Top.add(JourneyTrackerBox,1,0);
        Top.add(FuncButtons,2,0);


        layoutGame.setTop(Top);
    }

    private void refreshLeft() {
        VBox LeftMenu = new VBox();
        VBox UserData = new VBox(10);
        HBox MembersSelection = new HBox(40);
        MembersSelection.setAlignment(Pos.CENTER);
        for (CrewMember member : game.getMembers()) {
            String imageName;
            if (member.equals(game.getMember(0))) {
                imageName = "img/memberA.png";
            } else {
                imageName = "img/memberB.png";
            }
            Image MemberImage = new Image(imageName);
            ImageView image1 = new ImageView(MemberImage);
            image1.setFitWidth(0.02*SCREENWIDTH);
            image1.setFitHeight(0.03*SCREENHEIGHT);
            Label MemberName = new Label(member.getName());
            MemberName.setAlignment(Pos.CENTER);
            MemberName.getStyleClass().add("memberLabel");
            VBox UserInfo = new VBox(15);
            UserData.setAlignment(Pos.CENTER);
            UserData.fillWidthProperty();
            UserInfo.getChildren().addAll(image1, MemberName);
            UserInfo.getStyleClass().add("memberBox");
            UserInfo.setAlignment(Pos.CENTER);
            MembersSelection.getChildren().add(UserInfo);
        }


        VBox StockBox = new VBox(10);
        StockBox.setAlignment(Pos.CENTER);
        StockBox.fillWidthProperty();

        int dispersers = game.getDisperserUpdate();
        int organics = game.getDetonatorUpdate();
        int sealedToken = game.getSealedRoomUpdate();

        if (dispersers > 0) {
            Label stockLabel = new Label(dispersers + " x Particle Disperser");
            stockLabel.setAlignment(Pos.BASELINE_LEFT);
            stockLabel.getStyleClass().add("stockLabels");
            StockBox.getChildren().add(stockLabel);
        }
        if (organics > 0) {
            Label stockLabel = new Label(organics + " x Organic Detonator");
            stockLabel.setAlignment(Pos.BASELINE_LEFT);
            stockLabel.getStyleClass().add("stockLabels");
            StockBox.getChildren().add(stockLabel);
        }
        if (sealedToken > 0) {
            Label stockLabel = new Label(sealedToken + " x Sealed Room Token");
            stockLabel.setAlignment(Pos.BASELINE_LEFT);
            stockLabel.getStyleClass().add("stockLabels");
            StockBox.getChildren().add(stockLabel);
        }


        VBox Legends = new VBox(10);
        Legends.setAlignment(Pos.CENTER);
        Legends.fillWidthProperty();
        int iconSize = 20;

        ImageView HealthIcon = new ImageView(new Image("img/health.png"));
        HealthIcon.setFitHeight(iconSize);
        HealthIcon.setFitWidth(iconSize);
        HBox LegendHealth = new HBox();
        LegendHealth.setAlignment(Pos.CENTER);
        StackPane LegendHealthPane = new StackPane();
        LegendHealthPane.getChildren().add(HealthIcon);
        LegendHealthPane.getStyleClass().add("legendImage");
        Label LegendHealthLabel = new Label("Health");
        LegendHealthLabel.getStyleClass().add("legendText");
        LegendHealth.getChildren().addAll(LegendHealthPane, LegendHealthLabel);

        ImageView HullIcon = new ImageView(new Image("img/hull.png"));
        HullIcon.setFitHeight(iconSize);
        HullIcon.setFitWidth(iconSize);
        HBox LegendHull = new HBox();
        LegendHull.setAlignment(Pos.CENTER);
        StackPane LegendHullPane = new StackPane();
        LegendHullPane.getChildren().add(HullIcon);
        LegendHullPane.getStyleClass().add("legendImage");
        Label LegendHullLabel = new Label("Hull");
        LegendHullLabel.getStyleClass().add("legendText");
        LegendHull.getChildren().addAll(LegendHullPane, LegendHullLabel);

        ImageView AlienIcon = new ImageView(new Image("img/alien.png"));
        AlienIcon.setFitHeight(iconSize);
        AlienIcon.setFitWidth(iconSize);
        HBox LegendAlien = new HBox();
        LegendAlien.setAlignment(Pos.CENTER);
        StackPane LegendAlienPane = new StackPane();
        LegendAlienPane.getChildren().add(AlienIcon);
        LegendAlienPane.getStyleClass().add("legendImage");
        Label LegendAlienLabel = new Label("Alien");
        LegendAlienLabel.getStyleClass().add("legendText");
        LegendAlien.getChildren().addAll(LegendAlienPane, LegendAlienLabel);

        ImageView IPIcon = new ImageView(new Image("img/inspiration_points.png"));
        IPIcon.setFitHeight(iconSize);
        IPIcon.setFitWidth(iconSize);
        HBox LegendIP = new HBox();
        LegendIP.setAlignment(Pos.CENTER);
        StackPane LegendIPPane = new StackPane();
        LegendIPPane.getChildren().add(IPIcon);
        LegendIPPane.getStyleClass().add("legendImage");
        Label LegendIPLabel = new Label("Inspiration Points");
        LegendIPLabel.getStyleClass().add("legendText");
        LegendIP.getChildren().addAll(LegendIPPane, LegendIPLabel);

        ImageView DetonatorIcon = new ImageView(new Image("img/detonator.png"));
        DetonatorIcon.setFitHeight(iconSize);
        DetonatorIcon.setFitWidth(iconSize);
        HBox LegendDetonator = new HBox();
        LegendDetonator.setAlignment(Pos.CENTER);
        StackPane LegendDetonatorPane = new StackPane();
        LegendDetonatorPane.getChildren().add(DetonatorIcon);
        LegendDetonatorPane.getStyleClass().add("legendImage");
        Label LegendDetonatorLabel = new Label("Organic Detonator");
        LegendDetonatorLabel.getStyleClass().add("legendText");
        LegendDetonator.getChildren().addAll(LegendDetonatorPane, LegendDetonatorLabel);

        ImageView ParticleIcon = new ImageView(new Image("img/particle.png"));
        ParticleIcon.setFitHeight(iconSize);
        ParticleIcon.setFitWidth(iconSize);
        HBox LegendParticle = new HBox();
        LegendParticle.setAlignment(Pos.CENTER);
        StackPane LegendParticlePane = new StackPane();
        LegendParticlePane.getChildren().add(ParticleIcon);
        LegendParticlePane.getStyleClass().add("legendImage");
        Label LegendParticleLabel = new Label("Particle Disperser");
        LegendParticleLabel.getStyleClass().add("legendText");
        LegendParticle.getChildren().addAll(LegendParticlePane, LegendParticleLabel);


        Legends.getChildren().addAll(LegendHealth, LegendHull, LegendAlien, LegendIP, LegendDetonator, LegendParticle);

        UserData.getChildren().addAll(MembersSelection);
        UserData.setAlignment(Pos.TOP_CENTER);
        UserData.getStyleClass().add("leftPanel");
        StockBox.getStyleClass().add("leftPanel");
        Legends.getStyleClass().add("leftPanel");
        Legends.setAlignment(Pos.BOTTOM_CENTER);
        LeftMenu.getChildren().addAll(UserData, StockBox, Legends);
        LeftMenu.fillWidthProperty();
        LeftMenu.setPrefHeight(Region.USE_COMPUTED_SIZE);
        LeftMenu.getStyleClass().add("LeftMenu");

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(LeftMenu);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        layoutGame.setLeft(scrollPane);
    }

    private void refreshRight(){
        GameState state = game.getState();
        if (state instanceof SpendActionPoints){
            crewStore();
        }else if (state instanceof SpendInspirationPoints){
            restStore();
        }

    }

    private void refreshBottom() {
        ImageView HealthImage = new ImageView(new Image("img/health.png"));
        ImageView HullImage = new ImageView(new Image("img/hull.png"));
        ImageView AliensImage = new ImageView(new Image("img/alien.png"));
        ImageView IPImage = new ImageView(new Image("img/inspiration_points.png"));

        int ImageSizeY = 25;
        int ImageSizeX = 30;

        HealthImage.setFitHeight(ImageSizeY);
        HealthImage.setFitWidth(ImageSizeX);
        HullImage.setFitHeight(ImageSizeY);
        HullImage.setFitWidth(ImageSizeX);
        AliensImage.setFitHeight(ImageSizeY + 3);
        AliensImage.setFitWidth(ImageSizeX);
        IPImage.setFitHeight(ImageSizeY);
        IPImage.setFitWidth(ImageSizeX);



        GridPane Footer = new GridPane();
        Footer.getStyleClass().add("bottomPanel");
        HBox InfoBottom = new HBox(10);
        InfoBottom.setAlignment(Pos.CENTER);
        Label HealthPoints = new Label(String.valueOf(game.getHealth()));
        HealthPoints.getStyleClass().add("pointsLabels");
        Label HullPoints = new Label(String.valueOf(game.getHull()));
        HullPoints.getStyleClass().add("pointsLabels");

        Label AliensAlive = new Label(String.valueOf(game.getAliensTotalAlive()));
        AliensAlive.getStyleClass().add("pointsLabels");

        Label IPPoints = new Label(String.valueOf(game.getIP()));
        IPPoints.getStyleClass().add("pointsLabels");

        InfoBottom.getChildren().addAll(HealthImage, HealthPoints, HullImage, HullPoints, AliensImage, AliensAlive, IPImage, IPPoints);

        HBox FuncButtons = new HBox(10);
        FuncButtons.setAlignment(Pos.CENTER);
        Button advanceTracker = new Button("Avançar Jornada");
        advanceTracker.getStyleClass().add("btnAdvance");
        Button saveGame = new Button("Guardar Jogo");
        saveGame.getStyleClass().add("btnSave");

        advanceTracker.setOnAction(event -> {
            GameState state;
            game.spendActionPoints(0);
            System.out.println(game.getOutput());
            game.spendInspirationPoints(0);
            game.advanceTracker();
            state = game.getState();
            if(state instanceof SpendActionPoints){
                crewStore();
            }
            if(state instanceof SpendInspirationPoints){
                restStore();
            }
            redShirtSpecial();
            updateGame();
        });

        saveGame.setOnAction(event -> {
            try {
                game.saveGame();
                NotificationBox.display("Jogo guardado com sucesso!");
            }catch (Exception e){
                NotificationBox.display("Ocorreu um problema a guardar o jogo!");
            }
        });



        FuncButtons.getChildren().addAll(saveGame,advanceTracker);


        Footer.setAlignment(Pos.CENTER);
        Footer.add(InfoBottom,1,0);
        Footer.add(FuncButtons,2,0);
        ColumnConstraints columnBottom;
        columnBottom = new ColumnConstraints();
        columnBottom.setPercentWidth(20);
        Footer.getColumnConstraints().add(columnBottom);
        columnBottom = new ColumnConstraints();
        columnBottom.setPercentWidth(60);
        Footer.getColumnConstraints().add(columnBottom);
        columnBottom = new ColumnConstraints();
        columnBottom.setPercentWidth(20);
        Footer.getColumnConstraints().add(columnBottom);
        Footer.setVgap(2);
        Footer.setHgap(2);



        layoutGame.setBottom(Footer);
    }

    private void refreshCenter(){
        GridPane Map = new GridPane();
        CrewMember memberA = game.getMember(0);
        CrewMember memberB;
        try {
            memberB = game.getMember(1);
        }catch (Exception e){
            memberB = null;
        }


        int IconSizeX = 15;
        int IconSizeY = 18;
        for (Room room : game.getRooms()) {
            GridPane RoomGrid = new GridPane();
            int totalAliens = room.getAliensSize();

            StackPane ImageMemberAPane = new StackPane();
            if (room.hasMember(memberA)) {
                ImageView ImageMemberA = new ImageView(new Image("img/memberA.png"));
                ImageMemberA.setFitHeight(IconSizeY);
                ImageMemberA.setFitWidth(IconSizeX);
                ImageMemberAPane.getChildren().add(ImageMemberA);
                ImageMemberAPane.setAlignment(Pos.CENTER);
                GridPane.setHalignment(ImageMemberAPane, HPos.CENTER);
                GridPane.setValignment(ImageMemberAPane, VPos.CENTER);

            }
            RoomGrid.add(ImageMemberAPane, 0, 0);

            StackPane ImageMemberBPane = new StackPane();
            if (room.getNumber() == 5 || room.getNumber() == 8) {
                ImageMemberBPane.getStyleClass().add("roomDoorUp");
            }
            if(memberB != null) {
                if (room.hasMember(memberB)) {

                    ImageView ImageMemberB = new ImageView(new Image("img/memberB.png"));
                    ImageMemberB.setFitHeight(IconSizeY);
                    ImageMemberB.setFitWidth(IconSizeX);
                    ImageMemberBPane.getChildren().add(ImageMemberB);
                    ImageMemberBPane.setAlignment(Pos.CENTER);
                    GridPane.setHalignment(ImageMemberBPane, HPos.CENTER);
                    GridPane.setValignment(ImageMemberBPane, VPos.CENTER);

                }
                RoomGrid.add(ImageMemberBPane, 1, 0);
            }

            HBox AlienInfoBox = new HBox(2);
            if (totalAliens > 0) {
                ImageView ImageAlien = new ImageView(new Image("img/alien.png"));
                ImageAlien.setFitHeight(IconSizeY);
                ImageAlien.setFitWidth(IconSizeX);

                Label LabelTotalAliens = new Label(String.valueOf(totalAliens));
                LabelTotalAliens.setAlignment(Pos.TOP_RIGHT);
                LabelTotalAliens.getStyleClass().add("roomTotalAliens");
                AlienInfoBox.getChildren().addAll(ImageAlien, LabelTotalAliens);
                AlienInfoBox.setAlignment(Pos.CENTER);
                GridPane.setHalignment(AlienInfoBox, HPos.CENTER);
                GridPane.setValignment(AlienInfoBox, VPos.CENTER);

            }
            RoomGrid.add(AlienInfoBox, 2, 0);

            StackPane ImageDispersersPane = new StackPane();
            if (room.hasDispersers()) {
                ImageView ImageDispersers = new ImageView(new Image("img/particle.png"));
                ImageDispersers.setFitHeight(IconSizeY);
                ImageDispersers.setFitWidth(IconSizeX);
                ImageDispersersPane.getChildren().add(ImageDispersers);
                ImageDispersersPane.setAlignment(Pos.CENTER);
                GridPane.setHalignment(ImageDispersersPane, HPos.CENTER);
                GridPane.setValignment(ImageDispersersPane, VPos.CENTER);

            }
            RoomGrid.add(ImageDispersersPane, 0, 2);


            StackPane ImageOrganicsPane = new StackPane();
            if (room.hasOrganics()) {
                ImageView ImageOrganics = new ImageView(new Image("img/detonator.png"));
                ImageOrganics.setFitHeight(IconSizeY);
                ImageOrganics.setFitWidth(IconSizeX);
                ImageOrganicsPane.getChildren().add(ImageOrganics);
                ImageOrganicsPane.setAlignment(Pos.CENTER);
                GridPane.setHalignment(ImageOrganicsPane, HPos.CENTER);
                GridPane.setValignment(ImageOrganicsPane, VPos.CENTER);

            }
            RoomGrid.add(ImageOrganicsPane, 2, 2);


            Label LabelName = new Label(room.getName());
            LabelName.setAlignment(Pos.CENTER);
            LabelName.getStyleClass().add("roomName");
            HBox LabelNameBox = new HBox();
            LabelNameBox.setAlignment(Pos.CENTER);
            LabelNameBox.getChildren().add(LabelName);
            if (room.getNumber() == 3 || room.getNumber() == 12) {
                LabelNameBox.getStyleClass().add("roomDoorRight");
            }
            if (room.getNumber() == 8 || room.getNumber() == 4 || room.getNumber() == 7) {
                LabelNameBox.getStyleClass().add("roomDoorLeft");
            }
            GridPane.setHalignment(LabelNameBox, HPos.CENTER);
            GridPane.setValignment(LabelNameBox, VPos.CENTER);

            RoomGrid.add(LabelNameBox, 0, 1, 3, 1);

            Label LabelNumber = new Label(String.valueOf(room.getNumber()));
            LabelNumber.setAlignment(Pos.CENTER);
            LabelNumber.getStyleClass().add("roomNumber");
            HBox LabelNumberBox = new HBox();
            LabelNumberBox.setAlignment(Pos.CENTER);
//            LabelNumberBox.getChildren().add(LabelNumber);
            if (room.getNumber() != 6 && room.getNumber() != 12 && room.getNumber() != 7 && room.getNumber() != 1) {
                LabelNumberBox.getStyleClass().add("roomDoorDown");
            }
            GridPane.setHalignment(LabelNumberBox, HPos.CENTER);
            GridPane.setValignment(LabelNumberBox, VPos.CENTER);

            RoomGrid.add(LabelNumberBox, 1, 2);


            RoomGrid.setAlignment(Pos.CENTER);
            RoomGrid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            if (room.getSealed()) {
                RoomGrid.getStyleClass().add("roomSealed");
            } else {
                RoomGrid.getStyleClass().add("room");
            }

            RoomGrid.setAlignment(Pos.CENTER);
            RoomGrid.setPrefSize(SCREENWIDTH, SCREENHEIGHT);

            ColumnConstraints roomColumn;
            RowConstraints roomRow;
            for (int row = 0; row < 3; row++) {
                roomRow = new RowConstraints();
                roomRow.setPercentHeight((float) 100 / 3);
                RoomGrid.getRowConstraints().add(roomRow);
            }
            for (int col = 0; col < 3; col++) {
                roomColumn = new ColumnConstraints();
                roomColumn.setPercentWidth((float) 100 / 3);
                RoomGrid.getColumnConstraints().add(roomColumn);
            }
            RoomGrid.setPrefSize(SCREENWIDTH, SCREENHEIGHT);
            RoomGrid.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);

            switch (room.getNumber()) {
                case 1:
                    Map.add(RoomGrid, 0, 0, 4, 1);
                    break;
                case 3:
                    Map.add(RoomGrid, 0, 1);
                    break;
                case 5:
                    Map.add(RoomGrid, 1, 1, 1, 2);
                    break;
                case 8:
                    Map.add(RoomGrid, 2, 1, 1, 2);
                    break;
                case 4:
                    Map.add(RoomGrid, 3, 1);
                    break;
                case 9:
                    Map.add(RoomGrid, 0, 2, 1, 2);
                    break;
                case 11:
                    Map.add(RoomGrid, 3, 2, 1, 2);
                    break;
                case 10:
                    Map.add(RoomGrid, 1, 3, 1, 2);
                    break;
                case 2:
                    Map.add(RoomGrid, 2, 3, 1, 2);
                    break;
                case 12:
                    Map.add(RoomGrid, 0, 4);
                    break;
                case 7:
                    Map.add(RoomGrid, 3, 4);
                    break;
                case 6:
                    Map.add(RoomGrid, 0, 5, 4, 1);
                    break;
            }
        }

        Map.setAlignment(Pos.CENTER);


        ColumnConstraints column;
        RowConstraints row;

//        Map.setPrefSize(SCREENWIDTH, SCREENHEIGHT);
        Map.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        for (int r = 0; r < 6; r++) {
            row = new RowConstraints();
            row.fillHeightProperty();
            row.setPercentHeight((float) 100 / 6);
            Map.getRowConstraints().add(row);
        }
        for (int c = 0; c < 4; c++) {
            column = new ColumnConstraints();
            column.fillWidthProperty();
            column.setPercentWidth((float) 100 / 4);
            Map.getColumnConstraints().add(column);
        }
        Map.setVgap(2);
        Map.setHgap(2);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(Map);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        layoutGame.setCenter(scrollPane);
    }

    private void play() {
        updateGame();
        Scene gamePlay;
        ImageView HealthImage = new ImageView(new Image("img/health.png"));
        ImageView HullImage = new ImageView(new Image("img/hull.png"));
        ImageView AliensImage = new ImageView(new Image("img/alien.png"));
        ImageView IPImage = new ImageView(new Image("img/inspiration_points.png"));

        int ImageSizeY = 25;
        int ImageSizeX = 30;

        HealthImage.setFitHeight(ImageSizeY);
        HealthImage.setFitWidth(ImageSizeX);
        HullImage.setFitHeight(ImageSizeY);
        HullImage.setFitWidth(ImageSizeX);
        AliensImage.setFitHeight(ImageSizeY + 3);
        AliensImage.setFitWidth(ImageSizeX);
        IPImage.setFitHeight(ImageSizeY);
        IPImage.setFitWidth(ImageSizeX);

        refreshGame();


        layoutGame.getStylesheets().add("css/gamePlay.css");
        try {
            gamePlay = new Scene(layoutGame);
            gamePlay.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ESCAPE) {
                    if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                        window.setScene(mainMenu);
                        window.setMaximized(false);
                    }
                }
            });
            window.setScene(gamePlay);
        }catch (Exception e){
            System.out.println(e.toString());
        }
        window.setMaximized(true);
        window.show();
    }

    private void setup() {
        List<CrewMember> membersList = new ArrayList<>(12);
        List<Spinner> tracker = new ArrayList<>(13);
        List<CrewMember> selectedMembers = new ArrayList<>();

        window.setMaximized(true);
        BorderPane layout = new BorderPane();
        int iconSize = 20;

        membersList.add(new Captain());
        membersList.add(new Commander());
        membersList.add(new CommsOfficer());
        membersList.add(new Doctor());
        membersList.add(new Engineer());
        membersList.add(new MoralOfficer());
        membersList.add(new NavigationOfficer());
        membersList.add(new RedShirt());
        membersList.add(new ScienceOfficer());
        membersList.add(new SecurityOfficer());
        membersList.add(new ShuttlePilot());
        membersList.add(new TransporterChief());

        //Top Menu

        GridPane TopMenuGrid = new GridPane();
        TopMenuGrid.getStyleClass().add("topMenuGrid");
        Label GameName = new Label("Destination Earth - Configuração");
        GameName.getStyleClass().add("gameName");
        HBox FuncButtons = new HBox();
        FuncButtons.setAlignment(Pos.TOP_RIGHT);
        Button btnExit = new Button("X");
        btnExit.getStyleClass().add("btnExit");
        btnExit.setAlignment(Pos.CENTER);
        FuncButtons.getChildren().add(btnExit);
        FuncButtons.getStyleClass().add("funcButtons");

        btnExit.setOnAction(event -> {
            if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                window.setScene(mainMenu);
                window.setMaximized(false);
            }
        });

        TopMenuGrid.add(GameName, 0, 0);
        TopMenuGrid.add(FuncButtons, 1, 0);

        ColumnConstraints column;
        column = new ColumnConstraints();
        column.setPercentWidth(80);
        TopMenuGrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(20);
        TopMenuGrid.getColumnConstraints().add(column);


        layout.setTop(TopMenuGrid);

        //Left Menu

        VBox LeftMenu = new VBox(100);
        VBox leftMenuPath = new VBox(10);


        Button btnTracker = new Button("Configurar JourneyTracker");
        btnTracker.getStyleClass().add("btnLeftMenu");
        Button btnSelectMembers = new Button("Selecionar Membros");
        btnSelectMembers.getStyleClass().add("btnLeftMenu");
        Button btnSetFirstMember = new Button("Colocar Primeiro Membro");
        btnSetFirstMember.getStyleClass().add("btnLeftMenu");
        Button btnSetSecondMember = new Button("Colocar Segundo Membro");
        btnSetSecondMember.getStyleClass().add("btnLeftMenu");
        VBox trackerHelp = new VBox(10);


        ImageView ImageHelp = new ImageView(new Image("img/help.png"));
        ImageHelp.setFitHeight(iconSize);
        ImageHelp.setFitWidth(iconSize - 5);

        HBox ImageHelpBox = new HBox();
        ImageHelpBox.setAlignment(Pos.CENTER);
        StackPane ImageHelpPane = new StackPane();
        ImageHelpPane.getChildren().add(ImageHelp);
        ImageHelpPane.getStyleClass().add("legendImage");
        Label restHelp = new Label("Para inserir uma RestPhase, coloque 0");
        restHelp.getStyleClass().add("helpLabel");
        ImageHelpBox.getChildren().addAll(ImageHelpPane, restHelp);


        trackerHelp.getChildren().add(ImageHelpBox);

        leftMenuPath.getChildren().addAll(btnTracker, btnSelectMembers, btnSetFirstMember, btnSetSecondMember);
        LeftMenu.getChildren().add(leftMenuPath);
        LeftMenu.setAlignment(Pos.TOP_CENTER);
        LeftMenu.getStyleClass().add("leftMenu");

        LeftMenu.fillWidthProperty();
        LeftMenu.setPrefHeight(Region.USE_COMPUTED_SIZE);

        layout.setLeft(LeftMenu);

        //Tracker Selection

        BorderPane JourneyTrackerPane = new BorderPane();

        Label configTracker = new Label("Configure o seu journey tracker");
        configTracker.getStyleClass().add("memberSelectionLabel");
        configTracker.setPrefHeight(0.03*SCREENHEIGHT);
        configTracker.setAlignment(Pos.CENTER);
        configTracker.setMaxWidth(Double.MAX_VALUE);

        JourneyTrackerPane.setTop(configTracker);

        VBox JourneyTrackerBox = new VBox(20);
        JourneyTrackerBox.setAlignment(Pos.TOP_CENTER);
        JourneyTrackerBox.setFillWidth(true);
        JourneyTrackerBox.getStyleClass().add("tackerPanel");
        VBox JTButtonsBox = new VBox(10);
        JTButtonsBox.setAlignment(Pos.TOP_CENTER);
        JTButtonsBox.setFillWidth(true);
        Button trackerOriginal = new Button("Utilizar journey tracker padrão");
        trackerOriginal.getStyleClass().add("btnSelectTracker");
        trackerOriginal.setMaxWidth(Double.MAX_VALUE);
        Button trackerNew = new Button("Criar novo journey tracker");
        trackerNew.getStyleClass().add("btnSelectTracker");
        trackerNew.setMaxWidth(Double.MAX_VALUE);

        VBox newTrackerBox = new VBox(10);
        newTrackerBox.setAlignment(Pos.CENTER);
        newTrackerBox.getStyleClass().add("tackerPanel");

        trackerOriginal.setOnAction(event -> {
            if (!trackerOriginal.getStyleClass().contains("btnActive")) {
                trackerNew.getStyleClass().remove("btnActive");
                trackerOriginal.getStyleClass().add("btnActive");
            } else {
                trackerOriginal.getStyleClass().remove("btnActive");
            }
            newTrackerBox.getChildren().clear();
            LeftMenu.getChildren().remove(trackerHelp);
        });

        trackerNew.setOnAction(event -> {
            if (!trackerNew.getStyleClass().contains("btnActive")) {
                trackerOriginal.getStyleClass().remove("btnActive");
                trackerNew.getStyleClass().add("btnActive");
                LeftMenu.getChildren().add(trackerHelp);
                newTrackerBox.getChildren().clear();
                Label newTrackerHeader = new Label("Insira o número de aliens para nascer em cada posição");
                newTrackerHeader.getStyleClass().add("memberSelectionLabel");
                newTrackerHeader.setPrefHeight(0.03*SCREENHEIGHT);
                newTrackerHeader.setAlignment(Pos.CENTER);
                newTrackerHeader.setMaxWidth(Double.MAX_VALUE);
                newTrackerBox.getChildren().add(newTrackerHeader);
                for (int i = 0; i < 13; i++) {
                    HBox trackerPosBox = new HBox(5);
                    Label label = new Label("Posição #" + (i + 1));
                    label.getStyleClass().add("trackerPositionLabel");
                    label.setAlignment(Pos.CENTER);
                    HBox.setHgrow(label, Priority.ALWAYS);
                    Spinner<Integer> integerSpinner = new Spinner<>();
                    SpinnerValueFactory<Integer> settings = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 15, 0);
                    integerSpinner.setValueFactory(settings);
                    integerSpinner.getStyleClass().add("tackerSpiners");
                    integerSpinner.setPrefHeight(0.04*SCREENHEIGHT);


                    HBox.setHgrow(integerSpinner, Priority.ALWAYS);
                    integerSpinner.setMaxWidth(Double.MAX_VALUE);
                    trackerPosBox.getChildren().addAll(label, integerSpinner);
                    newTrackerBox.getChildren().add(trackerPosBox);
                    tracker.add(integerSpinner);
                }
            } else {
                newTrackerBox.getChildren().clear();
                trackerNew.getStyleClass().remove("btnActive");
                LeftMenu.getChildren().remove(trackerHelp);
            }
        });

        HBox JourneyTrackerFuncBtns = new HBox(10);
        Button btn_saveTracker = new Button("Guardar escolha");
        btn_saveTracker.getStyleClass().add("saveMembersBtn");
        btn_saveTracker.setAlignment(Pos.CENTER);
        HBox.setHgrow(btn_saveTracker, Priority.ALWAYS);
        btn_saveTracker.setMaxWidth(Double.MAX_VALUE);

        JourneyTrackerFuncBtns.setPrefWidth(Region.USE_COMPUTED_SIZE);
        JourneyTrackerFuncBtns.getChildren().add(btn_saveTracker);
        JourneyTrackerFuncBtns.setAlignment(Pos.CENTER);

        btn_saveTracker.setOnAction(event -> {
            if (trackerNew.getStyleClass().contains("btnActive")) {
                int pos = 1;
                int aliens = 0;
                Spinner old = null;
                for (Spinner position : tracker) {
                    int val = (Integer) position.getValue();
                    aliens += val;
                    if (pos > 1) {
                        if (val == 0 && (Integer) old.getValue() == 0) {
                            NotificationBox.display("Não podem existir duas RestPhase seguidas!");
                            return;
                        }
                        if (aliens >= 15 && val != 0) {
                            NotificationBox.display("A posição " + (pos) + " deve ser uma RestPhase uma vez que já inseriu 15 aliens");
                            return;
                        }
                        if (val == 0) {
                            aliens = 0;
                        }
                    }
                    old = position;
                    pos++;
                }
                List<TrackerPosition> journeyTrackerPositions = new ArrayList<>();
                journeyTrackerPositions.add(new TrackerPosition("Espaço", 0, false));
                for (Spinner position : tracker) {
                    int val = (Integer) position.getValue();
                    if (val == 0) {
                        journeyTrackerPositions.add(new TrackerPosition("D", 0, false));
                    } else {
                        journeyTrackerPositions.add(new TrackerPosition(String.valueOf(val), val, true));
                    }
                }
                journeyTrackerPositions.add(new TrackerPosition("Terra", 0, false));
                game.buildTracker(journeyTrackerPositions);
                NotificationBox.display("JourneyTracker Guardado com sucesso!");
                setup();
            } else if (trackerOriginal.getStyleClass().contains("btnActive")) {
                game.generateTracker();
                NotificationBox.display("JourneyTracker Guardado com sucesso!");
                setup();
            } else {
                NotificationBox.display("Selecione uma opção primeiro!");
            }
        });

        JTButtonsBox.getChildren().addAll(trackerOriginal, trackerNew);
        JourneyTrackerBox.setPadding(new Insets((0.01*SCREENHEIGHT), (0.05*SCREENWIDTH), (0.01*SCREENHEIGHT), (0.05*SCREENWIDTH)));
        JourneyTrackerBox.getChildren().addAll(JTButtonsBox, newTrackerBox);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(JourneyTrackerBox);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        JourneyTrackerPane.setCenter(scrollPane);
        JourneyTrackerPane.setBottom(JourneyTrackerFuncBtns);


        //Member Selection

        BorderPane MemberSelection = new BorderPane();

        HBox MemberSelectionMenu = new HBox(3);
        MemberSelectionMenu.getStyleClass().add("memberSelectionMenu");

        Label SelectMembers = new Label("Selecione dois membros");
        SelectMembers.getStyleClass().add("memberSelectionLabel");
        SelectMembers.setAlignment(Pos.CENTER);
        HBox.setHgrow(SelectMembers, Priority.ALWAYS);
        SelectMembers.setMaxWidth(Double.MAX_VALUE);


        GridPane MembersGrid = new GridPane();
        MembersGrid.getStyleClass().add("membersGrid");

        int memberRow = 0;
        int memberCol = 0;
        AtomicInteger members_chosen_total = new AtomicInteger();
        for (CrewMember member : membersList) {
            Button btn = new Button(member.getName());
            btn.setAlignment(Pos.CENTER);
            btn.getStyleClass().add("memberName");
            btn.setMaxWidth(Double.MAX_VALUE);
            btn.setMaxHeight(Double.MAX_VALUE);
            btn.setOnAction(event -> {
                if (!btn.getStyleClass().contains("btnActive")) {
                    if (members_chosen_total.get() < 2) {
                        btn.getStyleClass().add("btnActive");
                        members_chosen_total.getAndIncrement();
                        if (!selectedMembers.contains(member)) {
                            selectedMembers.add(member);
                        }
                    }
                } else {
                    btn.getStyleClass().remove("btnActive");
                    members_chosen_total.getAndDecrement();
                    selectedMembers.remove(member);
                }
            });
            MembersGrid.add(btn, memberCol, memberRow);
            if (memberCol == 3) {
                memberRow++;
                memberCol = -1;
            }
            memberCol++;
        }
        MembersGrid.setAlignment(Pos.CENTER);
        ColumnConstraints col;
        RowConstraints row;
        for (int c = 0; c < 4; c++) {
            col = new ColumnConstraints();
            col.setPercentWidth((float) 100 / 4);
            MembersGrid.getColumnConstraints().add(column);
        }
        for (int r = 0; r < 3; r++) {
            row = new RowConstraints();
            row.setPercentHeight((float) 100 / 3);
            MembersGrid.getRowConstraints().add(row);
        }
        MembersGrid.setPrefSize(SCREENWIDTH, SCREENHEIGHT); // Default width and height
        MembersGrid.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);


        MembersGrid.setPadding(new Insets((0.01*SCREENHEIGHT), (0.05*SCREENWIDTH), (0.01*SCREENHEIGHT), (0.05*SCREENWIDTH)));
        MembersGrid.setVgap(10);
        MembersGrid.setHgap(10);


        MemberSelectionMenu.setPrefWidth(Region.USE_COMPUTED_SIZE);
        MemberSelectionMenu.getChildren().add(SelectMembers);
        MemberSelectionMenu.setAlignment(Pos.CENTER);

        HBox MemberSelectionBtns = new HBox(10);
        Button btn_saveMembers = new Button("Guardar escolha");
        btn_saveMembers.getStyleClass().add("saveMembersBtn");
        btn_saveMembers.setAlignment(Pos.CENTER);
        HBox.setHgrow(btn_saveMembers, Priority.ALWAYS);
        btn_saveMembers.setMaxWidth(Double.MAX_VALUE);

        MemberSelectionBtns.setPrefWidth(Region.USE_COMPUTED_SIZE);
        MemberSelectionBtns.getChildren().add(btn_saveMembers);
        MemberSelectionBtns.setAlignment(Pos.CENTER);

        MemberSelection.setTop(MemberSelectionMenu);
        ScrollPane scrollPaneMembers = new ScrollPane();
        scrollPaneMembers.setContent(MembersGrid);
        scrollPaneMembers.setFitToHeight(true);
        scrollPaneMembers.setFitToWidth(true);
        MemberSelection.setCenter(scrollPaneMembers);
        MemberSelection.setBottom(MemberSelectionBtns);
        MemberSelection.setPrefWidth(Region.USE_COMPUTED_SIZE);

        btn_saveMembers.setOnAction(event -> {
            if (members_chosen_total.get() == 2) {
                for (CrewMember member : selectedMembers) {
                    game.addMember(member);
                }
                game.isMembersSet();
                NotificationBox.display("As suas opções foram guardadas!");
                setup();
            } else {
                NotificationBox.display("Selecione os dois membros primeiro!");
            }

        });
        GameState state = game.getState();
        if(state instanceof AwaitBeginning){
            btnSelectMembers.getStyleClass().remove("btnActive");
            btnSetFirstMember.getStyleClass().remove("btnActive");
            btnSetSecondMember.getStyleClass().remove("btnActive");
            btnTracker.getStyleClass().add("btnActive");
            layout.setCenter(JourneyTrackerPane);
        }else if (state instanceof AwaitMemberSelection){
            btnTracker.getStyleClass().remove("btnActive");
            btnSetFirstMember.getStyleClass().remove("btnActive");
            btnSetSecondMember.getStyleClass().remove("btnActive");
            btnSelectMembers.getStyleClass().add("btnActive");
            layout.setCenter(MemberSelection);
        }else if(state instanceof FirstMemberPlacement){
            btnTracker.getStyleClass().remove("btnActive");
            btnSelectMembers.getStyleClass().remove("btnActive");
            btnSetSecondMember.getStyleClass().remove("btnActive");
            btnSetFirstMember.getStyleClass().add("btnActive");
            try {
                layout.setCenter(setMemberInRoom(game.getMember(0),0));
            }catch (Exception e){
                NotificationBox.display("Selecione os membros antes de os colocar no sitio, não é?");
            }
        }else if(state instanceof SecondMemberPlacement){
            btnSelectMembers.getStyleClass().remove("btnActive");
            btnSetFirstMember.getStyleClass().remove("btnActive");
            btnTracker.getStyleClass().remove("btnActive");
            btnSetSecondMember.getStyleClass().add("btnActive");
            try {
                layout.setCenter(setMemberInRoom(game.getMember(1),1));
            }catch (Exception e){
                NotificationBox.display("Selecione os membros antes de os colocar no sitio, não é?");
            }
        }
        Scene GameSetup = new Scene(layout);


        GameSetup.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                if (ConfirmBox.display("Sair", "Deseja mesmo sair?")) {
                    window.close();
                }
            }
        });

        GameSetup.getStylesheets().add("css/gameSetup.css");
        window.setScene(GameSetup);
        window.setMaximized(true);
        window.show();
    }

    private void canChangeRoom(CrewMember member, VBox closeRooms) {
        turn++;
        if (member.getMovement() == 0) {
            for (Room room : game.getRooms()) {
                if (!room.getSealed() && !room.equals(game.getMemberRoom(member))) {
                    Button btnRoom = new Button(room.getName());
                    btnRoom.getStyleClass().add("btnSelectRoom");
                    HBox.setHgrow(btnRoom, Priority.ALWAYS);
                    btnRoom.setMaxWidth(Double.MAX_VALUE);
                    closeRooms.getChildren().add(btnRoom);
                    btnRoom.setOnAction(event2 -> {
                        game.spendActionPoints(1);
                        game.removeMemberFromRoom(member,game.getMemberRoom(member),room,turn);
                        change_rooms--;
                        if(change_rooms > 0) {
                            NotificationBox.display("Pode mover o " + member.getName() + " mais " + change_rooms + " vazes de graça!");
                        }
                        updateGame();
                    });
                }
            }
        } else {
            for (Room room : game.getMemberCloseRooms(member)) {
                if (!room.getSealed()) {
                    Button btnRoom = new Button(room.getName());
                    btnRoom.getStyleClass().add("btnSelectRoom");
                    HBox.setHgrow(btnRoom, Priority.ALWAYS);
                    btnRoom.setMaxWidth(Double.MAX_VALUE);
                    closeRooms.getChildren().add(btnRoom);
                    btnRoom.setOnAction(event2 -> {
                        game.spendActionPoints(1);
                        game.removeMemberFromRoom(member,game.getMemberRoom(member),room,turn);
                        change_rooms--;
                        if(change_rooms > 0) {
                            NotificationBox.display("Pode mover o " + member.getName() + " mais " + change_rooms + " vazes de graça!");
                        }
                        updateGame();
                    });
                }
            }
        }
    }

    private BorderPane setMemberInRoom(CrewMember member, int pos){
        BorderPane FirstMemberSetPane = new BorderPane();
        Label FirstMemberSetLabel = new Label("Selecione a sala para o "+member.getName());
        FirstMemberSetLabel.getStyleClass().add("memberSelectionLabel");
        FirstMemberSetLabel.setAlignment(Pos.CENTER);
        FirstMemberSetLabel.setMaxWidth(Double.MAX_VALUE);
        FirstMemberSetPane.setTop(FirstMemberSetLabel);

        VBox firstMemberRooms = new VBox(10);
        List<Room> selectedRoom = new ArrayList<>();
        for (Room room:game.getRooms()) {
            Button btnRoom = new Button(room.getName());
            btnRoom.getStyleClass().add("btnRoom");
            btnRoom.setAlignment(Pos.CENTER);
            HBox.setHgrow(btnRoom, Priority.ALWAYS);
            btnRoom.setMaxWidth(Double.MAX_VALUE);
            btnRoom.setOnAction(event -> {
                selectedRoom.clear();
                selectedRoom.add(room);

            });
            firstMemberRooms.getChildren().add(btnRoom);
        }
        firstMemberRooms.setPrefWidth( Region.USE_COMPUTED_SIZE);
        firstMemberRooms.setAlignment(Pos.CENTER);
        firstMemberRooms.setPadding(new Insets((0.01*SCREENHEIGHT), (0.05*SCREENWIDTH), (0.01*SCREENHEIGHT), (0.05*SCREENWIDTH)));
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(firstMemberRooms);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        FirstMemberSetPane.setCenter(scrollPane);

        Button btn_saveFirstMemberSet = new Button("Guardar escolha");
        btn_saveFirstMemberSet.getStyleClass().add("saveMembersBtn");
        btn_saveFirstMemberSet.setAlignment(Pos.CENTER);
        HBox.setHgrow(btn_saveFirstMemberSet, Priority.ALWAYS);
        btn_saveFirstMemberSet.setMaxWidth(Double.MAX_VALUE);
        btn_saveFirstMemberSet.setOnAction(event -> {
            try {
                game.putMemberInRoom(member,selectedRoom.get(0));
                NotificationBox.display("O membro "+member.getName()+" foi colocado na sala "+selectedRoom.get(0).getName());
                if(pos == 0){
                    game.isFirstMemberPlaced(selectedRoom.get(0),member);
                    setup();
                }
                if(pos == 1){
                    game.isSecondMemberPlaced(selectedRoom.get(0),member);
                    play();
                }
            }catch (Exception e){
                NotificationBox.display("Selecione uma sala antes de guardar!");
            }

        });
        FirstMemberSetPane.setBottom(btn_saveFirstMemberSet);

        return FirstMemberSetPane;
    }

    private void redShirtSpecial(){
        boolean exists = false;
        for (CrewMember member:game.getMembers()) {
            if(member instanceof RedShirt){
                exists = true;
            }
        }
        if (exists){
            if(ConfirmBox.display("RedShirt","Deseja abdicar do RedShirt por 5 de vida?")){
                if(game.killRedShirt()){
                    NotificationBox.display("O membro RedShirt foi removido, a vida dos seus tripulantes é agora "+game.getHealth());
                }else{
                    NotificationBox.display("O tripulante RedShirt continua em jogo!");
                }
            }else{
                NotificationBox.display("O tripulante RedShirt continua em jogo!");
            }
        }
        updateGame();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(REFRESH == 0) {
            refreshGame();
            REFRESH++;
        }

    }
}
